<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');

// Include WB admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Include the ordering class
//require(LEPTON_PATH.'/framework/class.order.php');

// Get new order
$order = new LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
$position = $order->get_new($section_id);
if(isset($_REQUEST['group_id'])){
	$group_id=$_REQUEST['group_id'];
}else{
	$group_id='0';
}

// Check if the copyright field shall be filled with the default value
$query_settings = $database->query("SELECT copyright_default, copyright_auto FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
$fetch_settings = $query_settings->fetchRow();
if ($fetch_settings['copyright_auto'] == 1) {
    $copyright = $fetch_settings['copyright_default'];
} else {
    $copyright = '';
}

// Insert new row into database
$database->query("INSERT INTO ".TABLE_PREFIX."mod_gallery_images (section_id,page_id,position,active,group_id,copyright) VALUES ('$section_id','$page_id','$position','1','$group_id','$copyright')");

// Get the id
$image_id = $database->get_one("SELECT LAST_INSERT_ID()");

// Say that a new record has been added, then redirect to modify page
if($database->is_error()) {
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&section_id='.$section_id.'&image_id='.$image_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&section_id='.$section_id.'&image_id='.$image_id);
}

// Print admin footer
$admin->print_footer();

?>