<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
require('../../config/config.php');
require('info.php');

// Include WB admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

require_once(LEPTON_PATH.'/modules/gallery/function_pngthumb.php');

// Load Language file
if(LANGUAGE_LOADED) {
	if(!file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
		require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
	} else {
		require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
	}
}

	$row = 'a';

$add = 0;
if (array_key_exists('add', $_POST)) {
    $add = (int) $_POST['add'];
}

switch($add){
	case 0:

		$page_id = (int) $_GET['page_id'];
		$section_id = (int) $_GET['section_id'];
        $group_id = 0;
        if (array_key_exists('group_id', $_GET)) {
            $group_id = (int) $_GET['group_id'];
        }
		?>
		<form name="modify" action="<?php echo LEPTON_URL; ?>/modules/gallery/add_several_images.php" method="post" enctype="multipart/form-data" style="margin: 0;">
		<input type="hidden" name="add" value="1" />
		<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
		<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
		<input type="hidden" name="group_id" value="<?php echo $group_id; ?>" />
		<h2><?php echo $module_name,' - ', $page_id; ?></h2>
		<table class="row_a" cellpadding="2" cellspacing="0" border="0" width="100%">
			<tr>
				<td colspan="2"><h3><?php echo $GTEXT['ADD_SEVERAL_PICS']; ?></h3></td>
			</tr>
			<tr>
				<td class="setting_name" width="25%"><?php echo $GTEXT['NUMBER_OF_NEW_PICS']; ?>:</td>
				<td class="setting_name"><input type="text" name="number_pics" value="" style="width: 10%;" maxlength="3" /></td>
			</tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td align="left">
					<input name="save" type="submit" value="<?php echo $GTEXT['CONTINUE']; ?>" style="width: 100px; margin-top: 5px;" />
				</td>
				<td align="right">
					<input type="button" value="<?php echo $TEXT['CANCEL']; ?>" onclick="javascript: window.location = '<?php echo ADMIN_URL; ?>/pages/modify.php?page_id=<?php echo $page_id; ?>';" style="width: 100px; margin-top: 5px;" />
				</td>
			</tr>
		</table>
        </form>
		<?php


	break;
	case 1:

		$page_id = (int) $_POST['page_id'];
		$group_id = (int) $_POST['group_id'];
		$section_id = (int) $_POST['section_id'];
		$number_pics = (int) $_POST['number_pics'];
        
        // Set default value for Copyright field?
        $query_settings = $database->query("SELECT copyright_default, copyright_auto FROM " . TABLE_PREFIX . "mod_gallery_settings WHERE section_id = '$section_id'");
        $fetch_settings = $query_settings->fetchRow();
        $copyright_default = '';
        if ($fetch_settings['copyright_auto'] == 1) {
            $copyright_default = $fetch_settings['copyright_default'];
        }
		?>

		<form name="modify" action="<?php echo LEPTON_URL; ?>/modules/gallery/add_several_images.php" method="post" enctype="multipart/form-data" style="margin: 0;">

		<input type="hidden" name="add" value="2" />
		<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
		<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
		<input type="hidden" name="group_id" value="<?php echo $group_id; ?>" />
		<input type="hidden" name="number_pics" value="<?php echo $number_pics; ?>" />
		<h2><?php echo $module_name,' - ', $page_id; ?></h2>
		<table  class="row_a" cellpadding="2" cellspacing="0" border="0" width="98%">
			<tr>
			<td colspan="4"><h3><?php echo $GTEXT['ADD_SEVERAL_PICS']; ?></h3></td>
		</tr>
			<tr>
			<td colspan="4" width="100%">
				<table border="0" cellpadding="2" cellspacing="0" width="100%">
				<?php for($i=1;$i<=$number_pics;$i++){ ?>
					<tr class="row_<?php echo $row; ?>">
						<td colspan="2"><big><b><?php echo $GTEXT['IMAGE']; ?> <?php echo $i?>:</b></big></td>
					</tr>
					<tr class="row_<?php echo $row; ?>">
						<td class="setting_name" width="15%"><?php echo $TEXT['TITLE']; ?>:</td>
						<td class="setting_name"><input type="text" name="title_<?php echo $i?>" value="" style="width: 98%;" maxlength="255" /></td>
					</tr>
                    <tr class="row_<?php echo $row; ?>">
						<td class="setting_name" width="15%"><?php echo $TEXT['IMAGE']; ?>:</td>
						<td class="setting_name"><input type="file" name="image_<?php echo $i?>" size="104"/></td>
					</tr>
					<tr class="row_<?php echo $row; ?>">
						<td class="setting_name" width="15%" ><?php echo $TEXT['DESCRIPTION']; ?>:</td>
						<td class="setting_name"><textarea name="description_<?php echo $i?>" id="description" style="width: 60%; height: 50px;"></textarea></td>
					</tr>
                    <tr class="row_<?php echo $row; ?>">
						<td class="setting_name" width="15%" ><?php echo $GTEXT['COPYRIGHT']; ?>:</td>
						<td class="setting_name"><input type="text" name="copyright_<?php echo $i?>" value="<?php echo htmlspecialchars($copyright_default)?>" style="width: 98%;" maxlength="255" /></td>
					</tr>                    
					<tr class="row_<?php echo $row; ?>">
						<td class="setting_name" width="15%"><?php echo $TEXT['ACTIVE']; ?>:</td>
						<td class="setting_name"><input type="radio" name="active_<?php echo $i?>" id="active_true_<?php echo $i?>" value="1" checked />
							<a href="#" onclick="javascript: document.getElementById('active_true_<?php echo $i?>').checked = true;">
							<?php echo $TEXT['YES']; ?>
							</a>
							&nbsp;
							<input type="radio" name="active_<?php echo $i?>" id="active_false_<?php echo $i?>" value="0" />
							<a href="#" onclick="javascript: document.getElementById('active_false_<?php echo $i?>').checked = true;">
							<?php echo $TEXT['NO']; ?>
							</a>
						</td>
					</tr>
					<tr class="row_<?php echo $row; ?>">
						<td colspan="2"><hr /></td>
					</tr>
					<?php
					if($row == 'a') {
					$row = 'b';
				} else {
					$row = 'a';
				} 
				}
				?>
				</table>
			</td>
		</tr>
		</table>

		<table cellpadding="0" cellspacing="0" border="0" width="98%">
			<tr>
				<td align="left">
					<input name="save" type="submit" value="<?php echo $TEXT['SAVE']; ?>" style="width: 100px; margin-top: 5px;" />
				</td>
				<td align="right">
					<input type="button" value="<?php echo $TEXT['CANCEL']; ?>" onclick="javascript: window.location = '<?php echo ADMIN_URL; ?>/pages/modify.php?page_id=<?php echo $page_id; ?>';" style="width: 100px; margin-top: 5px;" />
				</td>
			</tr>
		</table>
        </form>
		<?php

	break;
	case 2:
		// Include the ordering class
		//require(LEPTON_PATH.'/framework/class.order.php');

		$page_id = (int) $_POST['page_id'];
		$group_id = (int) $_POST['group_id'];
		$section_id = (int) $_POST['section_id'];
		$number_pics = (int) $_POST['number_pics'];
		
		// Include WB functions file
		require(LEPTON_PATH.'/framework/summary.functions.php');
        
		// For each picture inserted, do the following things:
		for($pic=1;$pic<=$number_pics;$pic++){	
			$title=addslashes($_POST['title_'.$pic]);
			//$title=addslashes($title);
			$image="image_$pic";
			$_FILES['image']=$_FILES[$image];
			$description=addslashes($_POST['description_'.$pic]);
			//$description=addslashes($$description);
			$active=$_POST['active_'.$pic];
            $copyright=addslashes($_POST['copyright_'.$pic]);
			//$active=$$active;
			// Get new order
			$order = new LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
			$position = $order->get_new($section_id);

			// Insert new row into database
			$database->query("INSERT INTO ".TABLE_PREFIX."mod_gallery_images (section_id,page_id,position,active,group_id,title,copyright) VALUES ('$section_id','$page_id','$position','1','$group_id','$title','$copyright')");

			
			// Get the id
			$image_id = $database->get_one("SELECT LAST_INSERT_ID()");

			// Say that a new record has been added, then redirect to modify page
			if($database->is_error()) {
				$admin->print_error($database->get_error(), LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&amp;section_id='.$section_id.'&amp;image_id='.$image_id);
			} else {
				//$image_id 

				$query_page = $database->query("SELECT level,link FROM ".TABLE_PREFIX."pages WHERE page_id = '$page_id'");
				$page = $query_page->fetchRow();
				$page_level = $page['level'];
				$page_link = $page['link'];
                $old_link  = "";

				$image_link = PAGES_DIRECTORY.'/gallery/'.page_filename($title).$image_id;

				// Make sure the post link is set and exists
				// Make news post access files dir
				make_dir(LEPTON_PATH.PAGES_DIRECTORY.'/gallery/');
				if(!is_writable(LEPTON_PATH.PAGES_DIRECTORY.'/gallery/')) {
					$admin->print_error($MESSAGE['PAGES']['CANNOT_CREATE_ACCESS_FILE']);
				} elseif($old_link != $image_link OR !file_exists($image_link)) {
					// We need to create a new file
					// First, delete old file if it exists
					if(file_exists(LEPTON_PATH.$old_link.'.php')) {
						unlink(LEPTON_PATH.$old_link.'.php');
					}
					// Specify the filename
					$filename = LEPTON_PATH.'/'.$image_link.'.php';
					// The depth of the page directory in the directory hierarchy
					// '/pages' is at depth 1
					$pages_dir_depth=count(explode('/',PAGES_DIRECTORY))-1;
					// Work-out how many ../'s we need to get to the index page
					$index_location = '../';
					for($i = 0; $i < $pages_dir_depth; $i++) {
						$index_location .= '../';
					}
					// Write to the filename
					$content = ''.
					'<?php
					$page_id = '.$page_id.';
					$section_id = '.$section_id.';
					$image_id = '.$image_id.';
					define("IMAGE_ID", $image_id);
					require("'.$index_location.'config.php");
					require(LEPTON_PATH."/index.php"); 
					?>';
				
					$handle = fopen($filename, 'w');
					fwrite($handle, $content);
					fclose($handle);
					change_mode($filename);
				}
				// Check if the user uploaded an image or wants to delete one
				if(isset($_FILES['image']['tmp_name']) AND $_FILES['image']['tmp_name'] != '') {
					// Get resize information and image directory
					$query_settings = $database->query("SELECT thmb_resize, thumb_max, main_max, image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
					$fetch_settings = $query_settings->fetchRow();
                    $image_dir      = trim($fetch_settings['image_dir'], '/');
                    $image_digits   = $fetch_settings['image_digits'];
					$resize = $fetch_settings['thmb_resize'];
					$thumb_max = $fetch_settings['thumb_max'];
                    
					// Get real filename and set new filename
					$filename = $_FILES['image']['name'];
					$path_parts = pathinfo($filename);
					$fileext = strtolower($path_parts['extension']);
					$new_filename = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$fileext;
					
					// Make sure the image is a jpg file
					if(!($fileext == "jpg" || $fileext == "jpeg" || $fileext == "png")) {
						$admin->print_error($MESSAGE['GENERIC']['FILE_TYPE'].' JPG / JPEG / PNG');
					}
					
					// Make sure the target directory exists
					make_dir(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir");
					
					// Upload image
					move_uploaded_file($_FILES['image']['tmp_name'], $new_filename);
					change_mode($new_filename);
					
                    // Check if we need to create a thumb                     
					if($resize != 0) {
						list($original_x, $original_y) = getimagesize($new_filename);		
						if($thumb_max=="1"){
							if ($original_x > $original_y){;}
							else { 
								$resize = $original_y*($resize/$original_x);
							};
						}
						if($thumb_max=="2"){
							if ($original_x < $original_y){;}
							else {
								$resize=$original_x*($resize/$original_y);
							};
						}

						// Resize the image
						$thumb_location = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$fileext;

						// Check thumbnail type
						if(!($fileext == "png")) {
							make_thumb($new_filename, $thumb_location, $resize);
						} else {
							make_thumb_png($new_filename, $thumb_location, $resize);
						}
						change_mode($thumb_location);
					}

					// Check if we need to create a main image
					$query_settings = $database->query("SELECT main_resize,main_max, thumb_max FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
					$fetch_settings = $query_settings->fetchRow();
					$resize = $fetch_settings['main_resize'];
					$thumb_max = $fetch_settings['thumb_max'];
					$main_max = $fetch_settings['main_max'];
					if($resize != 0) {
						list($original_x, $original_y) = getimagesize($new_filename);		
						
						if($thumb_max=="1"){
							if ($original_x > $original_y){;}
							else {
								$resize = floor($original_y*($resize/$original_x));
							};
						}
						if($thumb_max=="2"){
							if ($original_x < $original_y){;}
							else {
								$resize=floor($original_x*($resize/$original_y));
							};
						}

						// Resize the image
						$main_location = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/main".sprintf("%0${image_digits}u", $image_id).'.'.$fileext;
						
						// Check thumbnail type
						if(!($fileext == "png")) {
							make_thumb($new_filename, $main_location, $resize);
						} else {
							make_thumb_png($new_filename, $main_location, $resize);
						}
						change_mode($main_location);
					}
					
					// Update file extension in the database
					$database->query("UPDATE ".TABLE_PREFIX."mod_gallery_images SET extension = '$fileext' WHERE image_id = '$image_id'");
				}
				if(isset($_POST['delete_image']) AND $_POST['delete_image'] != '') {
					// Query the database for the image extension
					$query_content = $database->query("SELECT extension FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
					$fetch_content = $query_content->fetchRow();
					$ext = $fetch_content['extension'];

					// Try unlinking image
					if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) {
						unlink(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext);
					}
				}

				// Update row
				// Alt Title isn't set right! It should be $alttitle! (But for Fast-Imageuploading it works anyway..)
				$database->query("UPDATE ".TABLE_PREFIX."mod_gallery_images SET title = '$title', link = '$image_link', `description` = '$description', active = '$active', modified_when = '".time()."', modified_by = '".$admin->get_user_id()."', alttitle = '$title' WHERE image_id = '$image_id'");

				// Check if there is a db error, otherwise say successful
                $suc = -1;
				if($database->is_error()) {
					$admin->print_error($database->get_error(), LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&amp;section_id='.$section_id.'&amp;image_id='.$id);
					$suc=0;
				} else {
					if($suc!=0){
						$suc=1;
					}
				}
			}
		}
		
		$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
	break;
}

// Print admin footer
$admin->print_footer();
?>
