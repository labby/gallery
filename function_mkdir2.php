<?php

// Recursive version of mkdir

function mkdir2($basedir, $path, $mode = 0777) {
    // Temporarily deactivate some error reporting to prevent
    // mkdir from throwing warnings if some weird paths are used
    $olderrorreporting = error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    $basedir = rtrim($basedir, DIRECTORY_SEPARATOR);
    $path    = trim($path, DIRECTORY_SEPARATOR);
    $dirs = explode(DIRECTORY_SEPARATOR , $path);
    $count = count($dirs);
    $path = '';
    for ($i = 0; $i < $count; ++$i) {
        $path .= DIRECTORY_SEPARATOR . $dirs[$i];
        if (!is_dir($basedir . $path) && !mkdir($basedir . $path, $mode)) {
            error_reporting($olderrorreporting);
            return false;
        }
    }
    error_reporting($olderrorreporting);
    return true;
}

?>