<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');

// Get id
if(!isset($_GET['image_id']) OR !is_numeric($_GET['image_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
} else {
	$image_id = $_GET['image_id'];
}

// Include WB admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

// Get post details
$query_details = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
if($query_details->numRows() > 0) {
	$get_details = $query_details->fetchRow();
} else {
	$admin->print_error($TEXT['NOT_FOUND'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Get image directory and number of digits
$query_settings = $database->query("SELECT image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
$fetch_settings = $query_settings->fetchRow();
$image_dir      = trim($fetch_settings['image_dir'], '/');
$image_digits   = $fetch_settings['image_digits'];

// Unlink post access file
if(is_writable(LEPTON_PATH.$get_details['link'].'.php')) {
	unlink(LEPTON_PATH.$get_details['link'].'.php');
}

// Get image file extension
$ext = $get_details['extension'];

// Delete any images if they exists
$thumb = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$ext;
$main = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/main".sprintf("%0${image_digits}u", $image_id).'.'.$ext;
$image = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext;
if(file_exists($thumb) AND is_writable($thumb)) { unlink($thumb); }
if(file_exists($main) AND is_writable($main)) { unlink($main); }
if(file_exists($image) AND is_writable($image)) { unlink($image); }

// Delete post
$database->query("DELETE FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id' LIMIT 1");

// Clean up ordering
//require(LEPTON_PATH.'/framework/class.order.php');
$order = new LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
$order->clean($section_id); 

// Check if there is a db error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/modify_post.php?page_id='.$page_id.'&amp;image_id='.$image_id);
} else {
	$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Print admin footer
$admin->print_footer();

?>
