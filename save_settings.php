<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');
require('function_mkdir2.php');

// Include WB admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}

// This code removes any php tags and adds slashes
$friendly = array('&lt;', '&gt;', '?php');
$raw = array('<', '>', '');
$header = addslashes(str_replace($friendly, $raw, $_POST['header']));
$image_loop = addslashes(str_replace($friendly, $raw, $_POST['image_loop']));
$footer = addslashes(str_replace($friendly, $raw, $_POST['footer']));
$image_header = addslashes(str_replace($friendly, $raw, $_POST['image_header']));
$image_footer = addslashes(str_replace($friendly, $raw, $_POST['image_footer']));
$subimage_header = addslashes(str_replace($friendly, $raw, $_POST['subimage_header']));
$subimage_footer = addslashes(str_replace($friendly, $raw, $_POST['subimage_footer']));
$images_per_page = $_POST['images_per_page'];
$num_cols = $_POST['num_cols'];
$ordering = $_POST['ordering'];
$orderby = $_POST['orderby'];
$imagelink = $_POST['imagelink'];
$main_max = $_POST['main_max'];
$thumb_max = $_POST['thumb_max'];
$image_dir = trim($_POST['image_dir'], '/');
$image_digits = $_POST['image_digits'];
if ($image_digits > 9) {
    $image_digits = 9;
}
$copyright_default = addslashes(str_replace($friendly, $raw, $_POST['copyright_default']));
$copyright_auto = $_POST['copyright_auto'];

/*
$fetch_content['ordering']
0 - ascending position
1 - descending position
2 - ascending title
3 - descending title
4 - ascending modified_when
5 - descending modified_when

orderby:
position=0
title=1
modified_when=2
*/
if($ordering==0 and $orderby==0){$ordering=0;}
if($ordering==1 and $orderby==0){$ordering=1;}
if($ordering==0 and $orderby==1){$ordering=2;}
if($ordering==1 and $orderby==1){$ordering=3;}
if($ordering==0 and $orderby==2){$ordering=4;}
if($ordering==1 and $orderby==2){$ordering=5;}

if(extension_loaded('gd') AND function_exists('imageCreateFromJpeg')) {
	$thmbResize = $_POST['thmb_resize'];
	$mainResize = $_POST['main_resize'];
	$main_max = $_POST['main_max'];
	$thumb_max = $_POST['thumb_max'];

} else {
	$thmbResize = '';
	$mainResize = '';
	$main_max = '';
	$thumb_max = '';

}

// Check if image dir exists or can be created
if (! is_dir(LEPTON_PATH . MEDIA_DIRECTORY . "/$image_dir")) {
    if (! mkdir2(LEPTON_PATH . MEDIA_DIRECTORY, "$image_dir")) {
        $admin->print_error($GTEXT['ERROR_CREATEDIR'] . ': ' . MEDIA_DIRECTORY . "/$image_dir", ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
    }
}

// Update settings
$database->query("UPDATE " . TABLE_PREFIX . "mod_gallery_settings SET header = '$header', image_loop = '$image_loop', footer = '$footer', images_per_page = '$images_per_page', image_header = '$image_header', image_footer = '$image_footer', thmb_resize = '$thmbResize', main_resize = '$mainResize', num_cols = '$num_cols', ordering = '$ordering', imagelink='$imagelink', subimage_header='$subimage_header', subimage_footer='$subimage_footer',main_max ='$main_max', thumb_max='$thumb_max', image_dir='$image_dir', image_digits='$image_digits', copyright_default='$copyright_default', copyright_auto='$copyright_auto' WHERE section_id = '$section_id'");

// Check if there is a db error, otherwise copy checked fields to other 
// Image Gallery's pages
if($database->is_error()) {
	$admin->print_error($database->get_error(), ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
} else {
    
    // $fields_to_copy contains an array with the field names which are to be copied
    // into the settings of other Image Gallery pages
    if (isset($_POST['fields_to_copy'])) {
        $sql = "";
        while (list($key, $field_to_copy) = each($_POST['fields_to_copy'])) {
            if ($key > 0) {
                $sql .= ", ";
            }
            $sql .= "$field_to_copy = '" . $$field_to_copy . "'";
        }
        $sql = "UPDATE " . TABLE_PREFIX . "mod_gallery_settings SET " . $sql;
        $database->query($sql);
    
        // Check if there is a db error, otherwise say successful
        if($database->is_error()) {
            $admin->print_error($database->get_error(), ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
        } else {
            $admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
        }
    } else {
       $admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
    }
}

// Print admin footer
$admin->print_footer();
?>