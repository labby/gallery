<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');
require('info.php');

// Include WB admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}

// Is there more than one "Image Gallery" page? Keep this information,
// we need it later to determine if the copy checkboxes shall be shown
$query_content = $database->query("SELECT page_id FROM " . TABLE_PREFIX . "mod_gallery_settings WHERE page_id > 0");
$multiple_galleries = ($query_content->numRows() > 1);

// Get header and footer
$query_content = $database->query("SELECT * FROM " . TABLE_PREFIX . "mod_gallery_settings WHERE section_id = '$section_id'");
$fetch_content = $query_content->fetchRow();
/*
$fetch_content['ordering']
0 - ascending position
1 - descending position
2 - ascending title
3 - descending title
4 - ascending when
5 - descending when

orderby:
position=0
title=1
when=2
*/

$main_max_x="";
$main_max_y="";
$main_max=$fetch_content['main_max']; 
if($main_max==""){$main_max_both="checked";}
	switch($main_max){
		case 0:
			$main_max_both="checked";
			break;
		case 1:
			$main_max_x="checked";
			break;
		case 2:
			$main_max_y="checked";
			break;
		}

$thumb_max_x="";
$thumb_max_y="";
$thumb_max=$fetch_content['thumb_max']; 
if($thumb_max==""){$thumb_max_both="checked";}
	switch($thumb_max){
		case 0:
			$thumb_max_both="checked";
			break;
		case 1:
			$thumb_max_x="checked";
			break;
		case 2:
			$thumb_max_y="checked";
			break;
		}

// Set raw html "<"s and ">"s to be replaced by friendly html code
$raw = array('<', '>');
$friendly = array('&lt;', '&gt;');

?>

<form name="modify" action="<?php echo LEPTON_URL; ?>/modules/gallery/save_settings.php" method="post" style="margin: 0;">

<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />

<!-- General settings -->
<h2><?php echo $module_name,' - ', $page_id; ?></h2>
<table class="row_a" cellpadding="2" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="2">			
			<h3><?php echo $GTEXT['GSETTINGS']; ?></h3>
		</td>
	</tr>
	<?php if(extension_loaded('gd') AND function_exists('imageCreateFromJpeg')) { /* Makes sure GD library is installed */ ?>
    <tr>
        <td class="setting_name" width="175">
            <?php echo $GTEXT['IMAGE_DIRECTORY']; ?>:
        </td>
		<td>
            <?php echo MEDIA_DIRECTORY . '/'; ?>
            <input type="text" name="image_dir" value="<?php echo $fetch_content['image_dir']; ?>" style="width: 300px;" maxlength="255" />
        </td>
    </tr>
    <tr>
        <td class="setting_name" width="175">
            <?php echo $GTEXT['IMAGE_DIGITS']; ?>:
        </td>
		<td>
            <input type="text" name="image_digits" value="<?php echo $fetch_content['image_digits']; ?>" style="width: 30px;" maxlength="1" />
        </td>
    </tr>    
	<tr>
		<td class="setting_name" width="175">
			<?php echo $GTEXT['MAIN_RESIZE_IMAGE_TO']; ?>:
		</td>
		<td>
			<input type="text" name="main_resize" value="<?php echo $fetch_content['main_resize']; ?>" style="width: 30px;" maxlength="3" /><small> Pixel </small> 
			<img src="maxb.png"><input type="radio" name="main_max" value="0" id="main_max_both" <?php echo$main_max_both; ?> /><label <label for="main_max_both"><?php echo $GTEXT['BOTHXY']; ?></label>&nbsp;
			<img src="maxx.png"><input type="radio" name="main_max" value="1" id="main_max_x" <?php echo$main_max_x; ?> /><label for="main_max_x"><?php echo $GTEXT['MAXX']; ?></label>&nbsp;
			<img src="maxy.png"><input type="radio" name="main_max" value="2" id="main_max_y" <?php echo$main_max_y; ?> /><label for="main_max_y"><?php echo $GTEXT['MAXY']; ?></label>
		</td>
	</tr>
	<tr>
		<td class="setting_name" width="175">
			<?php echo $GTEXT['THUMB_RESIZE_IMAGE_TO']; ?>:
		</td>
		<td>
			<input type="text" name="thmb_resize" value="<?php echo $fetch_content['thmb_resize']; ?>" style="width: 30px;" maxlength="3" /><small> Pixel </small> 
			<img src="maxb.png"><input type="radio" name="thumb_max" value="0" id="thumb_max_both" <?php echo$thumb_max_both; ?> /><label for="thumb_max_both"><?php echo $GTEXT['BOTHXY']; ?></label>&nbsp;
			<img src="maxx.png"><input type="radio" name="thumb_max" value="1" id="thumb_max_x" <?php echo$thumb_max_x; ?> /><label for="thumb_max_x"><?php echo $GTEXT['MAXX']; ?></label>&nbsp;
			<img src="maxy.png"><input type="radio" name="thumb_max" value="2" id="thumb_max_y" <?php echo$thumb_max_y; ?> /><label for="thumb_max_y"><?php echo $GTEXT['MAXY']; ?></label>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td class="setting_name" width="175">
			<?php echo $GTEXT['IMAGES_PER_PAGE']; ?>:
		</td>
		<td>
			<input type="text" name="images_per_page" value="<?php echo $fetch_content['images_per_page']; ?>" style="width: 30px" maxlength="3" /> 0 = <?php echo $TEXT['UNLIMITED']; ?>
		</td>
	</tr>
	<tr>
		<td class="setting_name" width="175">
			<?php echo $GTEXT['NUMBER_OF_COLUMNS']; ?>:
		</td>
		<td><select name="num_cols" style="width: 50px">
			<?php
			for($i = 1; $i <= 10; $i++) {
				if($fetch_content['num_cols'] == $i) {
					$selected = ' selected';
				} else { 
					$selected = '';
				}
				echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
			}
			?>
			</select>
		</td>
	</tr>
	<?php 
	$imagelink=$fetch_content['imagelink'];
	if($imagelink==""){
		$imagelink=0;
	}
	?>
	<tr>
		<td class="setting_name" width="175">
			<?php echo $GTEXT['IMAGELINK']; ?>:
		</td>
		<td>
            <select name="imagelink" style="width: 250px">
                <option value="0" <?php if ($imagelink==0) {echo "selected";}?>><? echo $GTEXT['NOLINK']; ?></option>
                <option value="1" <?php if ($imagelink==1) {echo "selected";}?>><? echo $GTEXT['PARENTLINK']; ?></option>
                <option value="2" <?php if ($imagelink==2) {echo "selected";}?>><? echo $GTEXT['NEWLINK']; ?></option>
                <option value="3" <?php if ($imagelink==3) {echo "selected";}?>><? echo $GTEXT['POPUPLINK']; ?></option>
            </select>
		</td>
	</tr>
	<tr>
	    <td class="setting_name" width="175">
			<?php echo $GTEXT['ORDERING']; ?>:
		</td>
	    <td width="175">
	        <select name="ordering" style="width: 250px">
            <?php
            if ($fetch_content['ordering'] == '0' or $fetch_content['ordering'] == '2' or $fetch_content['ordering'] == '4') {
                $selected_asc = 'selected';
                $selected_desc = '';
            } else {
                $selected_asc = '';
                $selected_desc = 'selected';
            }
            ?>
            <option value="0" <?php echo $selected_asc; ?>><?php echo $GTEXT['ASCENDING']; ?></option>
            <option value="1" <?php echo $selected_desc; ?>><?php echo $GTEXT['DESCENDING']; ?></option>
	        </select>
            
			&nbsp;&nbsp;&nbsp;<?php echo $GTEXT['ORDERBY']; ?>:
            
	        <select name="orderby" style="width: 150px">
            <?php
    		if ($fetch_content['ordering'] == '4' or $fetch_content['ordering'] == '5') {
                $selected_position = '';
                $selected_title = '';
                $selected_when = 'selected';
            } elseif ($fetch_content['ordering'] == '2' or $fetch_content['ordering'] == '3') {
                $selected_position = '';
                $selected_title = 'selected';
                $selected_when = '';
            } else {
                $selected_position = 'selected';
                $selected_title = '';
                $selected_when = '';
			}
            ?>
            <option value="0" <?php echo $selected_position; ?>><?php echo $GTEXT['POSITION']; ?></option>
            <option value="1" <?php echo $selected_title; ?>><?php echo $GTEXT['TITLE']; ?></option>
            <option value="2" <?php echo $selected_when; ?>><?php echo $GTEXT['WHEN']; ?></option>
	        </select>
	    </td>
	</tr>
    <?php
	$copyright_auto=$fetch_content['copyright_auto'];
	if($copyright_auto==""){
		$copyright_auto=0;
	}
	?>
    
    <tr>
        <td class="setting_name" width="175">
            <?php echo $GTEXT['COPYRIGHT_DEFAULT']; ?>:
        </td>
		<td>
            <input type="text" name="copyright_default" value="<?php echo htmlspecialchars($fetch_content['copyright_default']); ?>" style="width: 487px;" maxlength="255" />
        </td>
    </tr>
    <tr>
        <td class="setting_name" width="175">
            <?php echo $GTEXT['COPYRIGHT_AUTO']; ?>:
        </td>
        <td>
            <select name="copyright_auto" style="width: 250px">
                <option value="0" <?php if ($copyright_auto==0) {echo "selected";}?>><? echo $TEXT['NO']; ?></option>
                <option value="1" <?php if ($copyright_auto==1) {echo "selected";}?>><? echo $GTEXT['COPYRIGHT_AUTO_INITIALIZE']; ?></option>
                <option value="2" <?php if ($copyright_auto==2) {echo "selected";}?>><? echo $GTEXT['COPYRIGHT_AUTO_EMPTY']; ?></option>
            </select>
        </td>
    </tr>    
</table>

<!-- Layout settings -->
<table class="row_a" cellpadding="2" cellspacing="0" border="0" width="100%" style="margin-top: 5px;">
	<tr>
		<td class="setting_name" colspan="2">
			<h3><?php echo $GTEXT['LSETTINGS']; ?>
                <button class="help" type="button" value="<?php echo $MENU['HELP']; ?>" onclick="self.open ('<?php echo LEPTON_URL; ?>/modules/gallery/modify_settings_help.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>','_blank');" ><?php echo $MENU['HELP']; ?></button><br />
            </h3>
			<?php
                if ($multiple_galleries) {                  
                    echo '<input type="checkbox" name="dummy" value="1" checked="checked" disabled="disabled" />';
                    echo $GTEXT['HINT_CHECKBOX'] . '<br /><br />';
                }
            ?>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr /></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="header"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="header" id="header" />&nbsp;<?php } echo $TEXT['HEADER']; ?>:</label></td>
		<td><textarea name="header" style="width: 98%; height: 100px;"><?php echo stripslashes($fetch_content['header']); ?></textarea></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="image_loop"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="image_loop" id="image_loop" />&nbsp;<?php } echo $TEXT['LOOP']; ?>:</label></td>
		<td><textarea name="image_loop" style="width: 98%; height: 160px;"><?php echo stripslashes($fetch_content['image_loop']); ?></textarea></td>
	</tr>	
	<tr>
		<td class="setting_name" width="175"><label for="footer"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="footer" id="footer" />&nbsp;<?php } echo $GTEXT['FOOTER']; ?>:</label></td>
		<td><textarea name="footer" style="width: 98%; height: 120px;"><?php echo str_replace($raw, $friendly, stripslashes($fetch_content['footer'])); ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2"><hr /></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="image_header"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="image_header" id="image_header" />&nbsp;<?php } echo $TEXT['IMAGE'].' '.$TEXT['HEADER']; ?>:</label></td>
		<td><textarea name="image_header" style="width: 98%; height: 100px;"><?php echo str_replace($raw, $friendly, stripslashes($fetch_content['image_header'])); ?></textarea></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="image_footer"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="image_footer" id="image_footer" />&nbsp;<?php } echo $TEXT['IMAGE'].' '.$GTEXT['FOOTER']; ?>:</label></td>
		<td><textarea name="image_footer" style="width: 98%; height: 160px;"><?php echo str_replace($raw, $friendly, stripslashes($fetch_content['image_footer'])); ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2"><hr /></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="subimage_header"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="subimage_header" id="subimage_header" />&nbsp;<?php } echo $TEXT['IMAGE'].' '.$GTEXT['SUBHEAD']; ?>:</label></td>
		<td class="setting_name"><textarea name="subimage_header" style="width: 98%; height: 100px;"><?php echo str_replace($raw, $friendly, stripslashes($fetch_content['subimage_header'])); ?></textarea></td>
	</tr>
	<tr>
		<td class="setting_name" width="175"><label for="subimage_footer"><?php if ($multiple_galleries) { ?><input type="checkbox" name="fields_to_copy[]" value="subimage_footer" id="subimage_footer" />&nbsp;<?php } echo $TEXT['IMAGE'].' '.$GTEXT['SUBFOOT']; ?>:</label></td>
		<td class="setting_name"><textarea name="subimage_footer" style="width: 98%; height: 100px;"><?php echo str_replace($raw, $friendly, stripslashes($fetch_content['subimage_footer'])); ?></textarea></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>	
	
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	
	<tr>
		<td align="left">
			<button class="save" name="save" type="submit" value="<?php echo $TEXT['SAVE']; ?>" style="width: 100px; margin-top: 5px;"><?php echo $TEXT['SAVE']; ?></button>
		</td>
		<td align="right">
			<button class="cancel" type="button" value="<?php echo $TEXT['CANCEL']; ?>" onclick="javascript: window.location = '<?php echo ADMIN_URL; ?>/pages/modify.php?page_id=<?php echo $page_id; ?>';" style="width: 100px; margin-top: 5px;"><?php echo $TEXT['CANCEL']; ?></button>
		</td>
	</tr>
</table>
</form>

<?php

// Print admin footer
$admin->print_footer();

?>