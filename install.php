<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

if(defined('LEPTON_URL')) {
	$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_gallery_images`");
	$mod_gallery = 'CREATE TABLE `'.TABLE_PREFIX.'mod_gallery_images` ( '
					 . '`image_id` INT NOT NULL AUTO_INCREMENT,'
					 . '`section_id` INT NOT NULL DEFAULT \'0\','
					 . '`page_id` INT NOT NULL DEFAULT \'0\',		   	    '
					 . '`group_id` INT NOT NULL DEFAULT \'0\','
					 . '`active` INT NOT NULL DEFAULT \'0\','
					 . '`position` INT NOT NULL DEFAULT \'0\','
					 . '`title` VARCHAR(255) NOT NULL DEFAULT \'\','
					 . '`link` TEXT NOT NULL ,'
					 . '`extension` VARCHAR(255) NOT NULL DEFAULT \'\','
					 . '`description` TEXT NOT NULL ,'
                     . '`copyright` VARCHAR(255) NOT NULL DEFAULT \'\','
					 . '`modified_when` INT NOT NULL DEFAULT \'0\','
					 . '`modified_by` INT NOT NULL DEFAULT \'0\','
					 . '`alttitle` VARCHAR(255) NOT NULL DEFAULT \'\','
					 . 'PRIMARY KEY (image_id)'
                . ' )';
	$database->query($mod_gallery);
	
	$database->query("DROP TABLE IF EXISTS `".TABLE_PREFIX."mod_gallery_settings`");
	$mod_gallery = 'CREATE TABLE `'.TABLE_PREFIX.'mod_gallery_settings` ( '
					 . '`section_id` INT NOT NULL DEFAULT \'0\','
					 . '`page_id` INT NOT NULL DEFAULT \'0\','
					 . '`header` TEXT NOT NULL ,'
					 . '`image_loop` TEXT NOT NULL ,'
					 . '`footer` TEXT NOT NULL ,'
					 . '`images_per_page` INT NOT NULL DEFAULT \'0\','
					 . '`num_cols` INT NOT NULL DEFAULT \'0\','
					 . '`image_header` TEXT NOT NULL,'
					 . '`image_footer` TEXT NOT NULL ,'
					 . '`thmb_resize` INT NOT NULL DEFAULT \'0\','
					 . '`main_resize` INT NOT NULL DEFAULT \'0\','
					 . '`ordering` TINYINT(1) NOT NULL DEFAULT \'0\','
					 . '`imagelink` TINYINT(1) NOT NULL DEFAULT \'0\','
					 . '`subimage_header` TEXT NOT NULL ,'
					 . '`subimage_footer` TEXT NOT NULL ,'
 					 . '`main_max` VARCHAR(2) NOT NULL DEFAULT \'\','
 					 . '`thumb_max` VARCHAR(2) NOT NULL DEFAULT \'\','
					 . '`image_dir` VARCHAR(255) NOT NULL DEFAULT \'.gallery\','
                     . '`image_digits` TINYINT(1) NOT NULL DEFAULT \'1\','
                     . '`copyright_default` VARCHAR(255) NOT NULL DEFAULT \'\','
                     . '`copyright_auto` TINYINT(1) NOT NULL DEFAULT \'0\','
					 . 'PRIMARY KEY (section_id)'
                . ' )';
	$database->query($mod_gallery);
		
	// Insert info into the search table
	// Module query info
	$field_info = array();
	$field_info['page_id'] = 'page_id';
	$field_info['title'] = 'page_title';
	$field_info['link'] = 'link';
	$field_info['description'] = 'description';
	$field_info['modified_when'] = 'modified_when';
	$field_info['modified_by'] = 'modified_by';
	$field_info['alttext'] = 'alttext';
	$field_info = serialize($field_info);
	$database->query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('module', 'gallery', '$field_info')");
	// Query start
	$query_start_code = "SELECT [TP]pages.page_id, [TP]pages.page_title, [TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by FROM [TP]mod_gallery_images, [TP]mod_gallery_settings, [TP]pages WHERE ";
	$database->query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_start', '$query_start_code', 'gallery')");
	// Query body
	$query_body_code = "
	[TP]pages.page_id = [TP]mod_gallery_images.page_id AND [TP]mod_gallery_images.title LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_gallery_images.page_id AND [TP]mod_gallery_images.description LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_gallery_settings.page_id AND [TP]mod_gallery_settings.header LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_gallery_settings.page_id AND [TP]mod_gallery_settings.footer LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_gallery_settings.page_id AND [TP]mod_gallery_settings.image_header LIKE \'%[STRING]%\'
	OR [TP]pages.page_id = [TP]mod_gallery_settings.page_id AND [TP]mod_gallery_settings.image_footer LIKE \'%[STRING]%\'";
	$database->query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_body', '$query_body_code', 'gallery')");
	// Query end
	$query_end_code = "";	
	$database->query("INSERT INTO ".TABLE_PREFIX."search (name,value,extra) VALUES ('query_end', '$query_end_code', 'gallery')");
	
	// Insert blank row (there needs to be at least on row for the search to work)
	$database->query("INSERT INTO ".TABLE_PREFIX."mod_gallery_images (section_id,page_id) VALUES ('0', '0')");
	$database->query("INSERT INTO ".TABLE_PREFIX."mod_gallery_settings (section_id,page_id) VALUES ('0', '0')");

}

?>