<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');
require('info.php');

// Get id
if(!isset($_GET['image_id']) OR !is_numeric($_GET['image_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
} else {
	$image_id = $_GET['image_id'];
}

// Include WB admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}

// Get header and footer
$query_content = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
$fetch_content = $query_content->fetchRow();

// Get image directory
$query_settings    = $database->query("SELECT image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
$fetch_settings    = $query_settings->fetchRow();
$image_dir         = trim($fetch_settings['image_dir'], '/');
$image_digits      = $fetch_settings['image_digits'];

// Image file extension
$ext = $fetch_content['extension'];

// Get list of pictures on this page with group_id=0
$list_pics = array();
$query_this_pic = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
if($query_this_pic->numRows() > 0) {
	if($pic = $query_this_pic->fetchRow()) {
		$this_page_id=$pic['page_id'];
		$this_section_id=$pic['section_id'];
		$this_group_id=$pic['group_id'];
		$query_pics = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE page_id = '$this_page_id' AND section_id = '$this_section_id' AND group_id = '0' ORDER BY image_id ");
		if($query_pics->numRows() > 0) {
			while($pics = $query_pics->fetchRow()) {
				if ($image_id == $pics['image_id'] || $this_group_id == $pics['image_id']) {
					continue;
				}
				$list_pics[$pics['image_id']]=$pics['title'];
			}
		}
	}
}
if($this_group_id != '0') {
	$list_pics['-1']="--Main--";
}
// Get list of all gallery-sections
$list_pages = array();

// For each section ...
$query_sections = $database->query("SELECT * FROM ".TABLE_PREFIX."sections WHERE module = 'gallery' ORDER BY section_id ");
if($query_sections->numRows() > 0) {
	while($res = $query_sections->fetchRow()) {
		$res_page_id=$res['page_id'];
		$res_section_id=$res['section_id'];
		if($this_page_id == $res_page_id && $this_section_id == $res_section_id) {
			continue;
		}
		// ... get the page-data
		$query_pages = $database->query("SELECT * FROM ".TABLE_PREFIX."pages WHERE page_id = '".$res_page_id."' AND visibility != 'none' AND visibility != 'deleted' ");
		if($query_pages->numRows() > 0) {
			$page = $query_pages->fetchRow();
			$page_title=$page['page_title'];
			$visibility=$page['visibility'];
			if($visibility == 'privat' || $visibility == 'registered') {
				$access_denied = true;
				$viewing_groups = explode(',', $page['viewing_groups']);
				$viewing_users = explode(',', $page['viewing_users']);
				if($oLEPTON->is_authenticated() == true) {
					if(in_array($oLEPTON->get_group_id(), $viewing_groups) || (in_array($oLEPTON->get_user_id(), $viewing_users))) {
						$access_denied = false;
					}
				}
				if($access_denied) {
					continue;
				}
			}
			$list_pages["$res_page_id-$res_section_id"]=$page_title;
		}
	}
}

?>

<style type="text/css">
.setting_name {
	vertical-align: top;
}
</style>

<form name="modify" action="<?php echo LEPTON_URL; ?>/modules/gallery/save_image.php" method="post" enctype="multipart/form-data" style="margin: 0;">

<input type="hidden" name="section_id" value="<?php echo $section_id; ?>" />
<input type="hidden" name="page_id" value="<?php echo $page_id; ?>" />
<input type="hidden" name="image_id" value="<?php echo $image_id; ?>" />
<input type="hidden" name="link" value="<?php echo $fetch_content['link']; ?>" />
<h2><?php echo $module_name,' - ', $page_id; ?></h2>
<table class="row_a" cellpadding="2" cellspacing="0" border="0" width="100%">
	<tr>
		<td colspan="3">			
			<h3><?php echo $GTEXT['MODIFY_PIC']; ?></h3>		
		</td>
	</tr>
	<tr>
		<td class="setting_name" width="150">
			<?php echo $TEXT['TITLE']; ?>:
		</td>
		<td>
			<input type="text" name="title" value="<?php echo htmlspecialchars(stripslashes($fetch_content['title'])); ?>" style="width: 98%;" maxlength="255" />
		</td>
			<td></td>
		</tr>
	<tr>
		<td class="setting_name" width="150">
			<?php echo $GTEXT['ALTTITLE']; ?>:
		</td>
		<td>
			<input type="text" name="alttitle" value="<?php echo htmlspecialchars(stripslashes($fetch_content['alttitle'])); ?>" style="width: 98%;" maxlength="255" />
		</td>
			<td></td>
		</tr>
	<tr>
		<td class="setting_name" width="150">
			<?php echo $TEXT['IMAGE']; ?>:
		</td>
		<?php if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) { ?>
		<td>
			<a href="<?php echo LEPTON_URL.MEDIA_DIRECTORY . '/' . $image_dir . '/image' . sprintf("%0{$image_digits}u", $image_id).'.'.$ext ?>" target="_blank">
			<?php if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) { ?>
			<?php echo '<img src="'.LEPTON_URL.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$ext.'">';?>
			<?php }?>
			</a>
			&nbsp;
			<input type="checkbox" name="delete_image" id="delete_image" value="true" />
			<a href="javascript: toggle_checkbox('delete_image');"><?php echo $TEXT['DELETE']; ?></a>
		</td>
		<?php } else { ?>
		<td>
			<input type="file" name="image" size="70"/>
		</td>
		<?php } ?>
	</tr>

	<?php if($fetch_content['group_id']==0 and $fetch_content['title']!="") { ?>
	<tr>
		<td class="setting_name" width="150">
			<?php echo $GTEXT['ADDITIONAL']; ?>:
		</td>
		<td>
			<input type="button" value="<?php echo $GTEXT['ADD_PIC']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL; ?>/modules/gallery/add_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;group_id=<?php echo $image_id; ?>';" style="width: 45%;" />
			<input type="button" value="<?php echo $GTEXT['ADD_SEVERAL_PICS']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL; ?>/modules/gallery/add_several_images.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;group_id=<?php echo $image_id; ?>';" style="width: 45%;" />
		</td>
			<td></td>
		</tr>		
	<?php } ?>

	<tr>
		<td class="setting_name" width="150">
			<?php echo $TEXT['ACTIVE']; ?>:
		</td>
		<td><input type="radio" name="active" id="active_true" value="1" <?php if($fetch_content['active'] == 1) { echo ' checked'; } ?> />
			<a href="#" onclick="javascript: document.getElementById('active_true').checked = true;">
			<?php echo $TEXT['YES']; ?>
			</a>
			&nbsp;
			<input type="radio" name="active" id="active_false" value="0" <?php if($fetch_content['active'] == 0) { echo ' checked'; } ?> />
			<a href="#" onclick="javascript: document.getElementById('active_false').checked = true;">
			<?php echo $TEXT['NO']; ?>
			</a>
		</td>
			<td></td>
		</tr>
    
	<tr>
		<td class="setting_name" width="150">
			<?php echo $GTEXT['MOVETOPAGE']; ?>:
		</td>
		<td>
			<select name="move_to_page" onchange="javascript: toggle_viewers();" style="width: 200px;">
			<option value="0-0" selected><?php echo $TEXT['PLEASE_SELECT']; ?></option>
			<?php foreach($list_pages AS $key=>$value) {
				echo "<option value=\"$key\">$value</option>";
			} ?>
            </select>
		</td>
			<td></td>
		</tr>
	<tr>
		<td class="setting_name" width="150">
			<?php echo $GTEXT['MOVEUNDERPIC']; ?>:
		</td>
		<td>
			<select name="move_under_pic" onchange="javascript: toggle_viewers();" style="width: 200px;">
			<option value="0" selected><?php echo $TEXT['PLEASE_SELECT']; ?></option>
			<?php foreach($list_pics AS $key=>$value) {
				echo "<option value=\"$key\">$value</option>";
			} ?>
            </select>
		</td>
			<td></td>
		</tr>
</table>

<table cellpadding="2" cellspacing="0" border="0" width="100%">
	<tr>
		<td>
			<?php		
			$content = stripslashes(htmlspecialchars($fetch_content['description']));
			if (!defined('WYSIWYG_EDITOR') OR WYSIWYG_EDITOR=="none" OR !file_exists(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php')) {
				function show_wysiwyg_editor($name,$id,$content,$width,$height) {
					echo '<textarea name="'.$name.'" id="'.$id.'" style="width: '.$width.'; height: '.$height.';">'.$content.'</textarea>';
				}
			} else {
				$id_list=array("description");
				require(LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php');
			}		
			show_wysiwyg_editor("description","description",$content,"98%","400px");
			?>
		</td>
	</tr>
</table>

<table class="row_a" cellpadding="2" cellspacing="0" border="0" width="100%">
	<tr>
		<td class="setting_name" width="150">
            <?php echo $GTEXT['COPYRIGHT']; ?>:
		</td>
		<td>
			<input type="text" name="copyright" value="<?php echo htmlspecialchars(stripslashes($fetch_content['copyright'])); ?>" style="width: 98%;" maxlength="255" />
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="left">
			<input name="save" type="submit" value="<?php echo $TEXT['SAVE']; ?>" style="width: 100px; margin-top: 5px;" />
		</td>
		<td align="right">
			<input type="button" value="<?php echo $TEXT['CANCEL']; ?>" onclick="javascript: window.location = '<?php echo ADMIN_URL; ?>/pages/modify.php?page_id=<?php echo $page_id; ?>';" style="width: 100px; margin-top: 5px;" />
		</td>
	</tr>
</table>
</form>

<?php

// Print admin footer
$admin->print_footer();

?>
