<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');
require('info.php');

// Include WB admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}
?>
<h2><?php echo $module_name; ?></h2>
<h3><?php echo $MENU['HELP']; ?></h3>
<p><?php echo $GTEXT['HELP_INTRODUCTION']; ?></p>

    

<table class="helptable" width="98%" border="0" cellspacing="3" cellpadding="6">
    <tr><th><?php echo $GTEXT['TOKEN'] ?></th><th><?php echo $TEXT['DESCRIPTION'] ?></th></tr>
    
    <tr><td class="token">[PAGE_TITLE]</td>           <td><?php echo $TEXT['PAGE_TITLE'] ?></td></tr>
    
    <tr><td class="token">[THUMB]</td>                <td><?php echo $GTEXT['THUMB_IMAGE'] ?></td></tr>
    <tr><td class="token">[TITLE]</td>                <td><?php echo $TEXT['TITLE'] ?></td></tr>
    <tr><td class="token">[ALTTITLE]</td>             <td><?php echo $GTEXT['ALTTITLE'] ?></td></tr>
    
    
    <tr><td class="token">[LINK]</td>                 <td><?php echo $GTEXT['IMAGE_LINK'] ?></td></tr>
    <tr><td class="token">[PREVIOUS_PAGE_LINK]</td>   <td><?php echo $GTEXT['PREVIOUS_PAGE_LINK'] ?></td></tr>
    <tr><td class="token">[NEXT_PAGE_LINK]</td>       <td><?php echo $GTEXT['NEXT_PAGE_LINK'] ?></td></tr>
    
    <tr><td class="token">[DESCRIPTION]</td>          <td><?php echo $TEXT['DESCRIPTION'] ?></td></tr>
    <tr><td class="token">[COPYRIGHT]</td>            <td><?php echo $GTEXT['COPYRIGHT'] ?></td></tr>
    <tr><td class="token">[BACK]</td>                 <td><?php echo $GTEXT['THUMB_LINK'] ?></td></tr>
    <tr><td class="token">[DATE]</td>                 <td><?php echo $GTEXT['MODIFICATION_DATE'] ?></td></tr>
    <tr><td class="token">[TIME]</td>                 <td><?php echo $GTEXT['MODIFICATION_TIME'] ?></td></tr>
    <tr><td class="token">[PREVIOUS]</td>             <td><?php echo $GTEXT['PREVIOUS_IMAGE_LINK'] ?></td></tr>
    <tr><td class="token">[PREVIOUS_URL]</td>         <td><?php echo $GTEXT['PREVIOUS_IMAGE_URL'] ?></td></tr>
    <tr><td class="token">[PAGE_CURRENT_NO]</td>      <td><?php echo $GTEXT['PAGE_CURRENT_NO']  . ' (' . $GTEXT['FOR_EXAMPLE'] . ' "3")' ?></td></tr>
    <tr><td class="token">[PAGE_TOTAL_NO]</td>        <td><?php echo $GTEXT['PAGE_TOTAL_NO']  . ' (' . $GTEXT['FOR_EXAMPLE'] . ' "12")' ?></td></tr>
    <tr><td class="token">[OF]</td>                   <td><?php echo $GTEXT['IMAGE_NO'] . ' (' . $GTEXT['FOR_EXAMPLE'] . ' "3 ' . strtolower($TEXT['OF']) . ' 12", ' . $GTEXT['LANGUAGE_DEPENDENT'] . ")" ?></td></tr>
    <tr><td class="token">[NEXT]</td>                 <td><?php echo $GTEXT['NEXT_IMAGE_LINK'] ?></td></tr>
    <tr><td class="token">[NEXT_URL]</td>             <td><?php echo $GTEXT['NEXT_IMAGE_URL'] ?></td></tr>
    <tr><td class="token">[TEXT_OUT_OF]</td>          <td><?php echo '"' . $TEXT['OUT_OF'] . '" (' . $GTEXT['LANGUAGE_DEPENDENT'] . ")"?></td></tr>
    <tr><td class="token">[TEXT_OF]</td>              <td><?php echo '"' . $TEXT['OF'] . '" (' . $GTEXT['LANGUAGE_DEPENDENT'] . ")"?></td></tr>
    <tr><td class="token">[TEXT_BACK]</td>            <td><?php echo '"' . $TEXT['BACK'] . '" (' . $GTEXT['LANGUAGE_DEPENDENT'] . ")"?></td></tr>
    <tr><td class="token">[USERNAME]</td>             <td><?php echo $TEXT['USERNAME'] ?></td></tr>
</table>


<?php
// Print admin footer

$admin->print_footer();
?>