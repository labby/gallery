<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* Strings used in modify_settings.php */
$GTEXT['GSETTINGS'] = 'General Settings';
$GTEXT['IMAGE_DIRECTORY'] = 'Image Directory';
$GTEXT['IMAGE_DIGITS'] = 'Minimum number of digits in image filenames';
$GTEXT['MAIN_RESIZE_IMAGE_TO'] = 'Main Image Size';

$GTEXT['BOTHXY'] = 'Horizontal & Vertical';
$GTEXT['MAXX'] = 'Horizontal';
$GTEXT['MAXY'] = 'Vertical';

$GTEXT['THUMB_RESIZE_IMAGE_TO'] = 'Thumbnail Image Size';
$GTEXT['IMAGES_PER_PAGE'] = 'Thumbnail Images per Page';
$GTEXT['NUMBER_OF_COLUMNS'] = 'Number of Columns';

$GTEXT['IMAGELINK'] = 'Full Size Link';
$GTEXT['NOLINK'] = 'No Link';
$GTEXT['PARENTLINK'] = 'Current Window';
$GTEXT['NEWLINK'] = 'New Window';
$GTEXT['POPUPLINK'] = 'In a Pop-Up';

$GTEXT['ORDERING'] = 'Order';
$GTEXT['ASCENDING'] = 'Ascending';
$GTEXT['DESCENDING'] = 'Descending';

$GTEXT['ORDERBY'] = 'Order';
$GTEXT['POSITION'] = 'Manually';
$GTEXT['TITLE'] = 'By Title';
$GTEXT['WHEN'] = 'By Modify Date';


$GTEXT['COPYRIGHT_DEFAULT'] = 'Default Copyright Line';
$GTEXT['COPYRIGHT_AUTO'] = 'Insert automatically';
$GTEXT['COPYRIGHT_AUTO_INITIALIZE'] = 'Initialize field';
$GTEXT['COPYRIGHT_AUTO_EMPTY'] = 'Show if field empty';

$GTEXT['LSETTINGS'] = 'Layout Settings';
$GTEXT['HINT_CHECKBOX'] = 'Check the box to copy this field to all Image Gallery pages';
$GTEXT['FOOTER'] = 'Footer';
$GTEXT['SUBHEAD'] = 'Sub Image Header';
$GTEXT['SUBFOOT'] = 'Sub Image Footer';

/* Strings used in modify_settings_help.php */
$GTEXT['HELP_INTRODUCTION'] = 'This table describes the tokens which can be used in the fields below';   
$GTEXT['TOKEN'] = 'Token';
$GTEXT['THUMB_IMAGE'] = '&lt;IMG&gt; tag for Thumbnail Image';
$GTEXT['ALTTITLE'] = 'Alt Title';
$GTEXT['IMAGE_LINK'] = 'Link to image page';
$GTEXT['PREVIOUS_PAGE_LINK'] = 'Link to previous page';
$GTEXT['NEXT_PAGE_LINK'] = 'Link to next page';
$GTEXT['COPYRIGHT'] = 'Copyright';
$GTEXT['THUMB_LINK'] = 'Link to thumbnail page';
$GTEXT['MODIFICATION_DATE'] = 'Last image modification date';
$GTEXT['MODIFICATION_TIME'] = 'Last image modification time';
$GTEXT['PREVIOUS_IMAGE_LINK'] = 'Link to previous image page (full &lt;A&gt; tag)';
$GTEXT['PREVIOUS_IMAGE_URL'] = 'Link to previous image page (URL only)';
$GTEXT['PAGE_CURRENT_NO'] = 'Number of current pages';
$GTEXT['PAGE_TOTAL_NO'] = 'Total number of pages';
$GTEXT['FOR_EXAMPLE'] = 'e.g.';
$GTEXT['IMAGE_NO'] = 'Image number';
$GTEXT['LANGUAGE_DEPENDENT'] = 'language dependent';
$GTEXT['NEXT_IMAGE_LINK'] = 'Link to next image page (full &lt;A&gt; tag)';
$GTEXT['NEXT_IMAGE_URL'] = 'Link to next image page (URL only)';

/* Token */
$GTEXT['PREVIOUS'] = 'Previous Image';
$GTEXT['NEXT'] = 'Next Image';

/* Strings used in image handling pages */
$GTEXT['IMAGE'] = 'Image';
$GTEXT['ADDITIONAL'] = 'Additional Image';
$GTEXT['ADD_PIC'] = 'Add Pic';
$GTEXT['ADD_SEVERAL_PICS'] = 'Add Multiple Pics';
$GTEXT['MODIFY_PIC'] = 'Modify/Move/Delete Pic';
$GTEXT['NUMBER_OF_NEW_PICS'] = 'Number of Pics To Add';
$GTEXT['CONTINUE'] = 'Continue';
$GTEXT['MOVETOPAGE'] = 'Move to page';
$GTEXT['MOVEUNDERPIC'] = 'Move to group';
$GTEXT['ERROR_CREATEDIR'] = 'Could not create directory';


?>