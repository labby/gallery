<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

$module_description = 'Erzeugt eine einfache Bildergalerie mit Bildbeschreibungen auf der Homepage';
$module_long_description = 'Mit diesem Modul k&ouml;nnen Sie ganz einfach Bilder von Ihrer lokalen Festplatte zu einer Galerie auf der Homepage zusammenstellen. Das Modul &uuml;bernimmt dabei alle Aufgaben f�r Sie. Die Bilder werden beim Hochladen automatisch auf die von Ihnen gew&uuml;nschte Gr&ouml;&szlig;e reduziert.';

/* Strings used in modify_settings.php */
$GTEXT['GSETTINGS']	= 'Grundeinstellungen';
$GTEXT['IMAGE_DIRECTORY'] = 'Bildverzeichnis';
$GTEXT['IMAGE_DIGITS'] = 'Minimale Anzahl Ziffern in Bilddateinamen';
$GTEXT['MAIN_RESIZE_IMAGE_TO'] = 'Maximale Bildgr&ouml;&szlig;e';

$GTEXT['BOTHXY'] = 'Horizontal & Vertikal';
$GTEXT['MAXX'] = 'Horizontal';
$GTEXT['MAXY'] = 'Vertikal';

$GTEXT['THUMB_RESIZE_IMAGE_TO'] = 'Maximale Vorschaubildgr&ouml;&szlig;e';
$GTEXT['IMAGES_PER_PAGE'] = 'Vorschaubilder pro Seite';
$GTEXT['NUMBER_OF_COLUMNS'] = 'Bilder pro Zeile';

$GTEXT['IMAGELINK'] = 'Originalbild';
$GTEXT['NOLINK'] = 'Nicht anzeigen';
$GTEXT['PARENTLINK'] = 'Im selben Fenster anzeigen';
$GTEXT['NEWLINK'] = 'In einem neuen Fenster anzeigen';
$GTEXT['POPUPLINK'] = 'In einem Pop-Up &ouml;ffnen';

$GTEXT['ORDERING'] = 'Sortierrichtung';
$GTEXT['ASCENDING'] = 'Aufsteigend';
$GTEXT['DESCENDING'] = 'Absteigend';

$GTEXT['ORDERBY'] = 'Sortierfeld';
$GTEXT['POSITION'] = '(Manuell)';
$GTEXT['TITLE'] = 'Name';
$GTEXT['WHEN'] = '&Auml;nderungsdatum';

$GTEXT['COPYRIGHT_DEFAULT'] = 'Standardtext Copyright-Zeile';
$GTEXT['COPYRIGHT_AUTO'] = 'Automatisch einf�gen';
$GTEXT['COPYRIGHT_AUTO_INITIALIZE'] = 'Bei neuen Bildern vorbelegen';
$GTEXT['COPYRIGHT_AUTO_EMPTY'] = 'Anzeigen, falls Feld leer';

$GTEXT['LSETTINGS']	= 'Layouteinstellungen';
$GTEXT['HINT_CHECKBOX'] = 'Ankreuzen, um das Feld in alle Image Gallery-Seiten zu kopieren';
$GTEXT['FOOTER'] = 'Fu&szlig;zeile';
$GTEXT['SUBHEAD'] = 'Kopfzeile Zusatzbilder';
$GTEXT['SUBFOOT'] = 'Fu&szlig;zeile Zusatzbilder';

/* Strings used in modify_settings_help.php */
$GTEXT['HELP_INTRODUCTION'] = 'Diese Tabelle beschreibt die Token, die in den unten angegebenen Feldern verwendet werden k�nnen';
$GTEXT['TOKEN'] = 'Token';
$GTEXT['THUMB_IMAGE'] = '&lt;IMG&gt;-Tag f&uuml;r Vorschaubild';
$GTEXT['ALTTITLE'] = 'Alternativtext';
$GTEXT['IMAGE_LINK'] = 'Link zur Bildseite';
$GTEXT['PREVIOUS_PAGE_LINK'] = 'Link zur vorherigen Seite';
$GTEXT['NEXT_PAGE_LINK'] = 'Link zur n&auml;chsten Seite';
$GTEXT['COPYRIGHT'] = 'Copyright';
$GTEXT['THUMB_LINK'] = 'Link zur Seite mit den Vorschaubildern';
$GTEXT['MODIFICATION_DATE'] = 'Datum der letzten &Auml;nderung des Bildes';
$GTEXT['MODIFICATION_TIME'] = 'Uhrzeit der letzten &Auml;nderung des Bildes';
$GTEXT['PREVIOUS_IMAGE_LINK'] = 'Link zur vorherigen Bildseite (komplettes &lt;A&gt;-Tag)';
$GTEXT['PREVIOUS_IMAGE_URL'] = 'Link zur vorherigen Bildseite (nur URL der Seite)';
$GTEXT['PAGE_CURRENT_NO'] = 'Nummer der gerade angezeigten Seite';
$GTEXT['PAGE_TOTAL_NO'] = 'Gesamtanzahl Seiten';
$GTEXT['FOR_EXAMPLE'] = 'z.B.';
$GTEXT['IMAGE_NO'] = 'Bildnummer';
$GTEXT['LANGUAGE_DEPENDENT'] = 'sprachabh�ngig';
$GTEXT['NEXT_IMAGE_LINK'] = 'Link zur n�chsten Bildseite (komplettes &lt;A&gt;-Tag)';
$GTEXT['NEXT_IMAGE_URL'] = 'Link zur n�chsten Bildseite (nur URL)';

/* Token */
$GTEXT['PREVIOUS'] = 'Vorheriges Bild';
$GTEXT['NEXT'] = 'N&auml;chstes Bild';

/* Strings used in image handling pages */
$GTEXT['IMAGE'] = 'Bild';
$GTEXT['ADDITIONAL'] = 'Zusatzbilder';
$GTEXT['ADD_PIC'] = 'Bild hinzuf&uuml;gen';
$GTEXT['ADD_SEVERAL_PICS'] = 'Mehrere Bilder hinzuf&uuml;gen';
$GTEXT['MODIFY_PIC'] = 'Bild &auml;ndern/verschieben/entfernen';
$GTEXT['NUMBER_OF_NEW_PICS'] = 'Anzahl neue Bilder';
$GTEXT['CONTINUE'] = 'Weiter';
$GTEXT['MOVETOPAGE'] = 'Verschiebe auf Seite';
$GTEXT['MOVEUNDERPIC'] = 'Verschiebe in Gruppe';
$GTEXT['ERROR_CREATEDIR'] = 'Verzeichnis konnte nicht angelegt werden';

?>