<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

// Must include code to stop this file being accessed directly
if(defined('LEPTON_PATH') == false) { exit("Cannot access this file directly"); }

$header = addslashes('<table cellpadding="0" cellspacing="0" border="0" width="98%">
<tr>');
$image_loop = addslashes('<td class="thumb">
<a href="[LINK]" title="[ALTTITLE]">[THUMB]<br />[TITLE]</a></td>');
$footer = addslashes('</tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" width="98%" style="display: [DISPLAY_PREVIOUS_NEXT_LINKS]">
<tr>
<td width="35%" align="left">[PREVIOUS_PAGE_LINK]</td>
<td width="30%" align="center">[OF]</td>
<td width="35%" align="right">[NEXT_PAGE_LINK]</td>
</tr>
</table>');
$image_header = addslashes('<center>');
$image_footer = addslashes('<p style="padding-top:10px;">[TITLE]</p>
<p class="ig_copyright">© [COPYRIGHT]</p>
<br />
<table cellpadding="0" cellspacing="0" border="0" width="98%">
<tr style="display: [DISPLAY_PREVIOUS_NEXT_LINKS]">
<td width="35%" align="left">[PREVIOUS]</td>
<td width="30%" align="center">[PAGE_CURRENT_NO] [TEXT_OF] [PAGE_TOTAL_NO]</td>
<td width="35%" align="right">[NEXT]</td>
</tr>
<tr>
<td colspan="3" align="center"><br /><a href="[BACK]">« Zurück zur &Uuml;bersicht</a></td>
</tr>
</table>
</center>
<br />');
$images_per_page = '0';
$num_cols = '2';
$subimage_header = addslashes('<hr><table border="0"><tr><td align=center>');
$subimage_footer = addslashes('</td></tr></table><hr>');
$image_dir = '.gallery';
$image_digits = '1';
$copyright_default = '';
$copyright_auto = '0';
$imagelink = '0';

if(extension_loaded('gd') AND function_exists('imageCreateFromJpeg') AND function_exists('imageCreateFromPng')) {
	$thmb_resize = '320';
	$main_resize = '655';
} else {
	$thmb_resize = '';
	$main_resize = '';
}
$ordering = '0';

$database->query("INSERT INTO ".TABLE_PREFIX."mod_gallery_settings
		 (section_id,page_id,header,image_loop,footer,image_header,image_footer,images_per_page,num_cols,thmb_resize,main_resize,ordering,subimage_header,subimage_footer,image_dir,image_digits,copyright_default,copyright_auto,imagelink)
		 VALUES ('$section_id','$page_id','$header','$image_loop','$footer','$image_header','$image_footer','$images_per_page','$num_cols','$thmb_resize','$main_resize','$ordering','$subimage_header','$subimage_footer','$image_dir','$image_digits', '$copyright_default', '$copyright_auto', '$imagelink')");


?>