<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

require('../../config/config.php');
require('function_pngthumb.php');
require('function_mkdir2.php');

// Get id
if(!isset($_POST['image_id']) OR !is_numeric($_POST['image_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
} else {
	$id = $_POST['image_id'];
	$image_id = $id;
}

// Include WB admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require(LEPTON_PATH.'/modules/admin.php');

// Include WB functions file
require_once(LEPTON_PATH.'/framework/summary.functions.php');

// Include class order file
//require_once(LEPTON_PATH.'/framework/class.order.php');

// Validate all fields
if($admin->get_post('title') == '' AND $admin->get_post('url') == '') {
	$admin->print_error($MESSAGE['GENERIC']['FILL_IN_ALL'], LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&amp;section_id='.$section_id.'&amp;image_id='.$id);
} else {
	$title = addslashes($admin->get_post('title'));
	$alttitle = addslashes($admin->get_post('alttitle'));
	$description = addslashes($admin->get_post('description'));
	$copyright = addslashes($admin->get_post('copyright'));
	$active = $admin->get_post('active');
	$old_link = $admin->get_post('link');
	$move_to_page = $admin->get_post('move_to_page');
	$move_under_pic = $admin->get_post('move_under_pic');    
}

// Check if we have to move the image - don't move it if the user uploaded an image or wants to delete one
if($move_to_page != '0-0' && !isset($_FILES['image']['tmp_name']) && !isset($_POST['delete_image'])) {
	list($new_page_id, $new_section_id) = explode('-', $move_to_page);
	if($new_page_id == $page_id && $new_section_id == $section_id) {
		unset($new_page_id);
		unset($new_section_id);
	} else {
		$old_page_id = $page_id;
		$page_id = $new_page_id;
		$old_section_id = $section_id;
		$section_id = $new_section_id;
		$first_image_id = $image_id;
	}
} // Use either move_to_page or move_under_pic, but not both!
elseif($move_under_pic != '0' && !isset($_FILES['image']['tmp_name']) && !isset($_POST['delete_image'])) {
	$first_image_id = $image_id;
	if($move_under_pic == '-1') {
		$new_group_id = 0;
	} else {	
		$new_group_id = $move_under_pic;
	}
}

// Loop through images
$not_finished = false;
do {
    // Get page link URL
    $query_page = $database->query("SELECT level,link FROM ".TABLE_PREFIX."pages WHERE page_id = '$page_id'");
    $page = $query_page->fetchRow();
    $page_level = $page['level'];
    $page_link = $page['link'];

    // Work-out what the link should be
    $image_link = PAGES_DIRECTORY.'/gallery/'.save_filename($title).$image_id;

    // Make sure the post link is set and exists
    // Make news post access files dir
    make_dir(LEPTON_PATH.PAGES_DIRECTORY.'/gallery/');
    if(!is_writable(LEPTON_PATH.PAGES_DIRECTORY.'/gallery/')) {
        $admin->print_error($MESSAGE['PAGES']['CANNOT_CREATE_ACCESS_FILE']);
    } elseif($old_link != $image_link || !file_exists(LEPTON_PATH.$image_link || isset($new_page_id))) {
        // We need to create a new file
        // First, delete old file if it exists
        if(file_exists(LEPTON_PATH.$old_link.'.php')) {
            unlink(LEPTON_PATH.$old_link.'.php');
        }
        // Specify the filename
        $filename = LEPTON_PATH.'/'.$image_link.'.php';
        // The depth of the page directory in the directory hierarchy
        // '/pages' is at depth 1
        $pages_dir_depth=count(explode('/',PAGES_DIRECTORY))-1;
        // Work-out how many ../'s we need to get to the index page
        $index_location = '../';
        for($i = 0; $i < $pages_dir_depth; $i++) {
            $index_location .= '../';
        }
        // Write to the filename
        $content = ''.
        '<?php
        $page_id = '.$page_id.';
        $section_id = '.$section_id.';
        $image_id = '.$image_id.';
        define("IMAGE_ID", $image_id);
        require("'.$index_location.'config/config.php");
        require(LEPTON_PATH."/index.php"); 
        ?>';
        $handle = fopen($filename, 'w');
        fwrite($handle, $content);
        fclose($handle);
        change_mode($filename);
    }

    // Check if the user uploaded an image or wants to delete one
    if(isset($_FILES['image']['tmp_name']) AND $_FILES['image']['tmp_name'] != '') {
        // Get resize information and image directory
        $query_settings = $database->query("SELECT thmb_resize, thumb_max, main_max, image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
        $fetch_settings = $query_settings->fetchRow();
        $image_dir      = trim($fetch_settings['image_dir'], '/');
        $image_digits   = $fetch_settings['image_digits'];    
        $resize = $fetch_settings['thmb_resize'];
        $thumb_max = $fetch_settings['thumb_max'];
        
        // Get real filename and set new filename
        $filename = $_FILES['image']['name'];
        $path_parts = pathinfo($filename);
        $fileext = strtolower($path_parts['extension']);
        $new_filename = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id). ".$fileext";
        
        // Make sure the image is a jpg file
        if(!($fileext == "jpg" || $fileext == "jpeg" || $fileext == "png")) {
            $admin->print_error($MESSAGE['GENERIC']['FILE_TYPE'].' JPG / JPEG / PNG', LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&amp;section_id='.$section_id.'&amp;image_id='.$id);
        }
        
        // Make sure the target directory exists
        mkdir2(LEPTON_PATH . MEDIA_DIRECTORY, "$image_dir");
        
        // Upload image
        move_uploaded_file($_FILES['image']['tmp_name'], $new_filename);
        change_mode($new_filename);
        
        // Check if we need to create a thumb
        if($resize != 0) {
            list($original_x, $original_y) = getimagesize($new_filename);		
            if($thumb_max=="1"){
                if ($original_x > $original_y){;}
                else { 
                    $resize = $original_y*($resize/$original_x);
                };
            }
            if($thumb_max=="2"){
                if ($original_x < $original_y){;}
                else {
                    $resize=$original_x*($resize/$original_y);
                };
            }

            // Resize the image
            $thumb_location = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$fileext;

            // Check thumbnail type
            if(!($fileext == "png")) {
                make_thumb($new_filename, $thumb_location, $resize);
            } else {
                make_thumb_png($new_filename, $thumb_location, $resize);
            }
            change_mode($thumb_location);
        }

        // Check if we need to create a main image
        $query_settings = $database->query("SELECT main_resize,main_max, thumb_max FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
        $fetch_settings = $query_settings->fetchRow();
        $resize = $fetch_settings['main_resize'];
        $thumb_max = $fetch_settings['thumb_max'];
        $main_max = $fetch_settings['main_max'];
        if($resize != 0) {
            list($original_x, $original_y) = getimagesize($new_filename);		
            
            if($thumb_max=="1"){
                if ($original_x > $original_y){;}
                else {
                    $resize = floor($original_y*($resize/$original_x));
                };
            }
            if($thumb_max=="2"){
                if ($original_x < $original_y){;}
                else {
                    $resize=floor($original_x*($resize/$original_y));
                };
            }

            // Resize the image
            $main_location = LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/main".sprintf("%0${image_digits}u", $image_id).'.'.$fileext;
            
            // Check thumbnail type
            if(!($fileext == "png")) {
                make_thumb($new_filename, $main_location, $resize);
            } else {
                make_thumb_png($new_filename, $main_location, $resize);
            }
            change_mode($main_location);
        }
        
        // Update file extension in the database
        $database->query("UPDATE ".TABLE_PREFIX."mod_gallery_images SET extension = '$fileext' WHERE image_id = '$image_id'");
    }

    if(isset($_POST['delete_image']) AND $_POST['delete_image'] != '') {
        // Query the database for the image extension
        $query_content = $database->query("SELECT extension FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
        $fetch_content = $query_content->fetchRow();
        $ext = $fetch_content['extension'];
        
        $query_settings = $database->query("SELECT image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
        $fetch_settings = $query_settings->fetchRow();
        $image_dir      = trim($fetch_settings['image_dir'], '/');
        $image_digits   = $fetch_settings['image_digits'];

        // Try unlinking images
        if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) {
            unlink(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/image".sprintf("%0${image_digits}u", $image_id).'.'.$ext);
        }
        if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) {
            unlink(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/thumb".sprintf("%0${image_digits}u", $image_id).'.'.$ext);
        }
        if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/main".sprintf("%0${image_digits}u", $image_id).'.'.$ext)) {
            unlink(LEPTON_PATH.MEDIA_DIRECTORY."/$image_dir/main".sprintf("%0${image_digits}u", $image_id).'.'.$ext);
        }
    }
    
   	// Change some data (needed for move_to_page or move_under_pic)
	$query_pic = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
	$fetch_pic = $query_pic->fetchRow();
	$group_id = $fetch_pic['group_id'];
	$position = $fetch_pic['position'];
	if(isset($new_group_id) || isset($new_page_id)) {
		if(isset($new_group_id)) {
			$group_id = $new_group_id;
		}			
		elseif(isset($new_page_id)) {
			if($not_finished == false) {
				$group_id = 0;
			}
		}			
		// Get position in new section_id
		$order = new LEPTON_LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
		$position = $order->get_new($section_id);
	}
    
  	// Update row
  	$database->query("UPDATE ".TABLE_PREFIX."mod_gallery_images SET title = '$title', link = '$image_link', page_id = '$page_id', section_id = '$section_id', `description` = '$description', `copyright` = '$copyright',active = '$active', modified_when = '".time()."', modified_by = '".$admin->get_user_id()."', alttitle = '$alttitle', position = '$position', group_id = '$group_id' WHERE image_id = '$image_id'");
  	if($database->is_error()) {
  		$admin->print_error($database->get_error(), LEPTON_URL.'/modules/gallery/modify_image.php?page_id='.$page_id.'&amp;section_id='.$section_id.'&amp;image_id='.$id);
  	}

	if(isset($new_group_id)) {
		// Clean up ordering in section_id
		$order = new LEPTON_LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
		$order->clean($section_id); 
        
		// We have to move images with group_id==$first_image_id too!
		$query_pics = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE group_id = '$first_image_id' AND page_id = '$page_id' ORDER BY position ASC ");
		if($query_pics->numRows() > 0) {
			$not_finished = true;
			if($pic = $query_pics->fetchRow()) {
				$page_id = $pic['page_id'];
				$section_id = $pic['section_id'];
				$image_id = $pic['image_id'];
				$old_link = $pic['link'];
				$title = addslashes($pic['title']);
				$alttitle = addslashes($pic['alttitle']);
				$description = addslashes($pic['description']);
                $copyright = $pic['copyright'];
				$active = $pic['active'];
			}
		} else {
			$not_finished = false;
		}
	}
	elseif(isset($new_page_id)) {
        
        // Check if we have to move the image files into a new directory
        // or rename them. This is necessary if the image directories
        // and/or the number of digits of the old and new page are different.
        // Fortunately the image_ids are unique, so we don't have to find
        // a new one!
        $query = $database->query("SELECT image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE page_id = '$old_page_id'");
        $settings = $query->fetchRow();
        $old_image_dir = $settings['image_dir'];
        $old_image_digits = $settings['image_digits'];
        
        $query = $database->query("SELECT image_dir, image_digits FROM ".TABLE_PREFIX."mod_gallery_settings WHERE page_id = '$new_page_id'");
        $settings = $query->fetchRow();
        $new_image_dir = $settings['image_dir'];
        $new_image_digits = $settings['image_digits'];
        
               
        if (($old_image_dir <> $new_image_dir) || ($old_image_digits <> $new_image_digits)) {
            // Either the direcory name or the number of digits is different.
            // Move/rename the image files!
            
            // Fetch extension of the image
            $query = $database->query("SELECT extension FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '$image_id'");
            $result = $query->fetchRow();
            $ext = $result['extension'];

            // Determine the absolute filenames and rename the images
            $oldfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$old_image_dir/image" . sprintf("%0${old_image_digits}u", $image_id) . ".$ext";
            $newfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$new_image_dir/image" . sprintf("%0${new_image_digits}u", $image_id) . ".$ext";
            rename($oldfilename, $newfilename);
            
            $oldfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$old_image_dir/thumb" . sprintf("%0${old_image_digits}u", $image_id) . ".$ext";
            $newfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$new_image_dir/thumb" . sprintf("%0${new_image_digits}u", $image_id) . ".$ext";
            rename($oldfilename, $newfilename);
            
            $oldfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$old_image_dir/main" . sprintf("%0${old_image_digits}u", $image_id) . ".$ext";
            $newfilename = LEPTON_PATH.MEDIA_DIRECTORY . "/$new_image_dir/main" . sprintf("%0${new_image_digits}u", $image_id) . ".$ext";
            rename($oldfilename, $newfilename);
        } else {
            echo "No need to rename!\n";
        }
                
		// Clean up ordering in old section_id
		$order = new LEPTON_LEPTON_order(TABLE_PREFIX.'mod_gallery_images', 'position', 'image_id', 'section_id');
		$order->clean($old_section_id);
        
		// We have to move images with group_id==$first_image_id too!
		$query_pics = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE group_id = '$first_image_id' AND page_id = '$old_page_id' ORDER BY position ASC ");
		if($query_pics->numRows() > 0) {
			$not_finished = true;
			if($pic = $query_pics->fetchRow()) {
				$page_id = $new_page_id;
				$section_id = $new_section_id;
				$image_id = $pic['image_id'];
				$old_link = $pic['link'];
				$title = addslashes($pic['title']);
				$alttitle = addslashes($pic['alttitle']);
				$description = addslashes($pic['description']);
                $copyright = $pic['copyright'];
				$active = $pic['active'];
			}
		} else {
			$not_finished = false;
		}
	}
    
} while($not_finished);

// Print admin success message
$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);

// Print admin footer
$admin->print_footer();

?>
