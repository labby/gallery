<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

// Prevent this file from being accessed directly
if (!defined('LEPTON_PATH')) {
    exit("Cannot access this file directly");
}

require('info.php');

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}

$database->query("DELETE FROM ".TABLE_PREFIX."mod_gallery_images  WHERE page_id = '$page_id' and section_id = '$section_id' and title=''"); 
?>
<h2><?php echo $module_name,' - ', $page_id; ?></h2>
<table cellpadding="0" cellspacing="0" border="0" width="100%">

<tr>
	<td align="left" width="33%">
		<input type="button" value="<?php echo $GTEXT['ADD_PIC']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL; ?>/modules/gallery/add_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>';" style="width: 100%;" />
	</td>
	<td align="left" width="33%">
		<input type="button" value="<?php echo $GTEXT['ADD_SEVERAL_PICS']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL; ?>/modules/gallery/add_several_images.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>';" style="width: 100%;" />
	</td>
	<td align="right" width="33%">
		<input type="button" value="<?php echo $TEXT['SETTINGS']; ?>" onclick="javascript: window.location = '<?php echo LEPTON_URL; ?>/modules/gallery/modify_settings.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>';" style="width: 100%;" />
	</td>
</tr>
</table>

<br />

<h3><?php echo $GTEXT['MODIFY_PIC']; ?></h3>

<?php

// Get settings
$query_settings = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
$settings = $query_settings->fetchRow();

/*
$fetch_content['ordering']
0 - ascending position
1 - descending position
2 - ascending title
3 - descending title
4 - ascending modified_when
5 - descending modified_when

orderby:
position=0
title=1
modified_when=2
*/

// Replace Ordering value with DESC or ASC
$orderkey=$settings['ordering'];
if($orderkey == '4' or $orderkey == '5'){
	$orderby="modified_when";
}
elseif($orderkey =='2' or $orderkey == '3'){
	$orderby="title";
}
else {
	$orderby="position";
}

if ($orderkey == '0' or $orderkey == '2' or $orderkey == '4') {
	$ordering = 'ASC';
	$moveupfile='move_up.php';
	$movedownfile='move_down.php';
} else {
	$ordering = 'DESC';
	$moveupfile='move_down.php';
	$movedownfile='move_up.php';
}

$query_total_num = $database->query("SELECT image_id FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND title != '' and group_id='0'");
$total_num = $query_total_num->numRows();

// Loop through existing images
$query_images = $database->query("SELECT * FROM `".TABLE_PREFIX."mod_gallery_images` WHERE section_id = '$section_id' and group_id='0' ORDER BY $orderby $ordering");
if($query_images->numRows() > 0) {
	$num_images = $query_images->numRows();
	$row = 'a';
	?>
	<table cellpadding="2" cellspacing="0" border="0" width="100%">
	<?php
    $position = 0;
	while($post = $query_images->fetchRow()) {
		$position++;
    ?>
            
		<tr class="row_<?php echo $row; ?>" height="20">
			<td width="20" style="padding-left: 5px;">
				<a href="<?php echo LEPTON_URL; ?>/modules/gallery/modify_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $post['image_id']; ?>" title="<?php echo $TEXT['MODIFY']; ?>">
					<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/modify_16.png" border="0" alt="Modify - " />
				</a>
			</td>
			<td colspan=2>
				<a href="<?php echo LEPTON_URL; ?>/modules/gallery/modify_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $post['image_id']; ?>">
					<?php echo stripslashes($post['title']); ?>
				</a>
			</td>
			<td width="80">
				<?php
				echo $TEXT['ACTIVE'].': ';
				if($post['active'] == 1) {
					echo $TEXT['YES'];
				} else { 
					echo $TEXT['NO']; 
				}
				?>
			</td>
            
            <!-- Arrow up -->
            <td width="20">
            <?php if($position != 1 and ($orderkey==1 or $orderkey==0)) { ?>
				<a href="<?php echo LEPTON_URL; ?>/modules/gallery/<?PHP echo $moveupfile; ?>?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $post['image_id']; ?>" title="<?php echo $TEXT['MOVE_UP']; ?>">
					<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/up_16.png" border="0" alt="^" />
				</a>
			<?php } ?>
			</td>
            
            <!-- Arrow down -->
			<td width="20">
			<?php if($position != $num_images and ($orderkey==1 or $orderkey==0)) { ?>
				<a href="<?php echo LEPTON_URL; ?>/modules/gallery/<?PHP echo $movedownfile; ?>?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $post['image_id']; ?>" title="<?php echo $TEXT['MOVE_DOWN']; ?>">
					<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/down_16.png" border="0" alt="v" />
				</a>
			<?php } ?>
			</td>
            
			<?php
			$query_subimages = $database->query("SELECT * FROM `".TABLE_PREFIX."mod_gallery_images` WHERE section_id = '$section_id' and group_id='".$post['image_id']."' ORDER BY $orderby $ordering");
			if($query_subimages->numRows() > 0) { ?>
				<td>&nbsp;</td>
				<?php } else { ?>
				<td width="20">
					<a href="javascript: confirm_link('<?php echo $TEXT['ARE_YOU_SURE']; ?>', '<?php echo LEPTON_URL; ?>/modules/gallery/delete_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $post['image_id']; ?>');" title="<?php echo $TEXT['DELETE']; ?>">
						<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/delete_16.png" border="0" alt="X" />
					</a>
				</td>
				<?php }	?>
		</tr>
		<?php
        
        // Handle subimages
        
		if($query_subimages->numRows() > 0) {
			$num_subimages = $query_subimages->numRows();
            $subposition = 0;
			while($subpost = $query_subimages->fetchRow()) {
                $subposition++;
				if($row == 'a') {
					$row = 'b';
				} else {
					$row = 'a';
				} 
				?>		
				<tr  class="row_<?php echo $row; ?>" height="20">
					<td>&nbsp;</td>
					<td width="20" style="padding-left: 5px;">
						<a href="<?php echo LEPTON_URL; ?>/modules/gallery/modify_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $subpost['image_id']; ?>" title="<?php echo $TEXT['MODIFY']; ?>">
							<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/modify_16.png" border="0" alt="Modify - " />
						</a>
					</td>
					<td>
						<a href="<?php echo LEPTON_URL; ?>/modules/gallery/modify_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $subpost['image_id']; ?>">
							<?php echo stripslashes($subpost['title']); ?>
						</a>
					</td>
					<td width="80">
						<?php echo $TEXT['ACTIVE'].': '; if($subpost['active'] == 1) { echo $TEXT['YES']; } else { echo $TEXT['NO']; } ?>
					</td>
                    
                    <!-- Arrow up -->
					<td width="20">
					<?php if($subposition != 1 and ($orderkey==1 or $orderkey==0)) { ?>
						<a href="<?php echo LEPTON_URL; ?>/modules/gallery/<?PHP echo $moveupfile; ?>?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $subpost['image_id']; ?>" title="<?php echo $TEXT['MOVE_UP']; ?>">
							<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/up_16.png" border="0" alt="^" />
						</a>
					<?php } ?>
					</td>
                    
                    <!-- Arrow down -->
					<td width="20">
					<?php if($subposition != $num_subimages and ($orderkey==1 or $orderkey==0)) { ?>
						<a href="<?php echo LEPTON_URL; ?>/modules/gallery/<?PHP echo $movedownfile; ?>?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $subpost['image_id']; ?>" title="<?php echo $TEXT['MOVE_DOWN']; ?>">
							<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/down_16.png" border="0" alt="v" />
						</a>
					<?php } ?>
					</td>
		<td width="20">
						<a href="javascript: confirm_link('<?php echo $TEXT['ARE_YOU_SURE']; ?>', '<?php echo LEPTON_URL; ?>/modules/gallery/delete_image.php?page_id=<?php echo $page_id; ?>&amp;section_id=<?php echo $section_id; ?>&amp;image_id=<?php echo $subpost['image_id']; ?>');" title="<?php echo $TEXT['DELETE']; ?>">
							<img src="<?php echo LEPTON_URL; ?>/modules/lib_lepton/backend_images/delete_16.png" border="0" alt="X" />
						</a>
					</td>
	</tr>
				<?php
			} // endsubrowecho
		} // endsubrow
		if($row == 'a') {
			$row = 'b';
		} else {
			$row = 'a';
		} 
	}
	?>
	</table>
	<?php
} else {
	echo $TEXT['NONE_FOUND'];
}

?>
<br />