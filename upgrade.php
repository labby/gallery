<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

// Prevent this file from being accessed directly
if (!defined('LEPTON_PATH')) {
    exit("Cannot access this file directly");
}

//Include config.php
require_once('../../config/config.php');

// Adds $field to $table if it doesn't already exist
function db_add_field($field, $table, $desc) {
	global $database;
	$table = TABLE_PREFIX . $table;
    
    // Does the field already exist?
	$query = $database->query("DESCRIBE $table '$field'");
	if(!$query || $query->numRows() == 0) {
        // No: Add field
        $cSQL = "ALTER TABLE $table ADD $field $desc";
        // echo "$cSQL<br />\n";
        
		$query = $database->query($cSQL);
		// echo (mysql_error()?mysql_error().'<br />':'');
        
		$query = $database->query("DESCRIBE $table '$field'");
		// echo (mysql_error()?mysql_error().'<br />':'');
    } else {
        // Yes: Just print a message
        // echo "Field '$field' already present in table '$table'<br />\n";
    }
        
}

// Added in v1.9e_lk_01
db_add_field("image_dir",         "mod_gallery_settings", "VARCHAR(255) NOT NULL DEFAULT '.gallery'");
db_add_field("image_digits",      "mod_gallery_settings", "TINYINT(1)   NOT NULL DEFAULT '1'");

// Added in v1.9f_lk_01
db_add_field("copyright",         "mod_gallery_images",   "VARCHAR(255) NOT NULL DEFAULT '' AFTER description");
db_add_field("copyright_default", "mod_gallery_settings", "VARCHAR(255) NOT NULL DEFAULT ''");
db_add_field("copyright_auto",    "mod_gallery_settings", "TINYINT(1)   NOT NULL DEFAULT '0'");

// This file has been renamed in v1.91f_lk_01 and can be deleted
if (file_exists(LEPTON_PATH . "/modules/gallery/function_pngthumb.php")) {
    if (file_exists(LEPTON_PATH . "/modules/gallery/pngthumb.php")) {
        unlink(LEPTON_PATH . "/modules/gallery/pngthumb.php");
    }
}

// This useless file has been inadvertently put into one of the beta
// versions. Delete it.
if (file_exists(LEPTON_PATH . "/modules/gallery/help.txt")) {
    unlink(LEPTON_PATH . "/modules/gallery/help.txt");
}

echo "<br />\n";