<?php

/*

 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2007, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

// Must include code to stop this file being accessed directly
if(defined('LEPTON_PATH') == false) { exit("Cannot access this file directly"); }

// Load Language file
if(LANGUAGE_LOADED) {
    require_once(LEPTON_PATH.'/modules/gallery/languages/EN.php');
    if(file_exists(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php')) {
        require_once(LEPTON_PATH.'/modules/gallery/languages/'.LANGUAGE.'.php');
    }
}

// Define config vars
define('THUMB_PREPEND', '<img src="');
define('THUMB_APPEND', ' border="0" class="thumbnail ig_thumbnail" />');
define('NEW_ROW', '</tr><tr>');

// Check if there is a start point defined
if(isset($_GET['p']) AND is_numeric($_GET['p']) AND $_GET['p'] >= 0) {
	$position = $_GET['p'];
} else {
	$position = 0;
}

// Get user's username, display name, email, and id - needed for insertion into image info
$users = array();
$query_users = $database->query("SELECT user_id,username,display_name,email FROM ".TABLE_PREFIX."users");
if($query_users->numRows() > 0) {
	while($user = $query_users->fetchRow()) {
		// Insert user info into users array
		$user_id = $user['user_id'];
		$users[$user_id]['username'] = $user['username'];
		$users[$user_id]['display_name'] = $user['display_name'];
		$users[$user_id]['email'] = $user['email'];
	}
}

// Check if we should show the main page or a image itself
if(!defined('IMAGE_ID') OR !is_numeric(IMAGE_ID)) {
    // Display main page with thumbnails
	
	// Get settings
	$query_settings = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
	if($query_settings->numRows() > 0) {
		$fetch_settings = $query_settings->fetchRow();
		$setting_header = stripslashes($fetch_settings['header']);
		$setting_image_loop = stripslashes($fetch_settings['image_loop']);
		$setting_footer = stripslashes($fetch_settings['footer']);
		$setting_images_per_page = $fetch_settings['images_per_page'];
		$setting_num_cols = $fetch_settings['num_cols'];
		$setting_resize = stripslashes($fetch_settings['thmb_resize']);
		$setting_ordering = $fetch_settings['ordering'];
		$setting_imagelink = $fetch_settings['imagelink'];
        $setting_image_dir = trim($fetch_settings['image_dir'], '/');
        $setting_image_digits = $fetch_settings['image_digits'];
        $setting_copyright_default = stripslashes($fetch_settings['copyright_default']);
        $setting_copyright_auto = $fetch_settings['copyright_auto'];
	} else {
		$setting_header = '';
		$setting_image_loop = '';
		$setting_footer = '';
		$setting_images_per_page = '';
		$setting_num_cols = '';
		$setting_resize = '';
		$setting_ordering = '';
		$setting_imagelink='';
        $setting_image_dir = '';
        $setting_image_digits = 1;
        $setting_copyright_default = '';
        $setting_copyright_auto = 0;
	}
	if($setting_ordering==0){
		$ordering=0;
		$orderby="position";
	}
	if($setting_ordering==1){
		$ordering=1;
		$orderby="position";
	}
	if($setting_ordering==2){
		$ordering=0;
		$orderby="title";
	}
	if($setting_ordering==3){
		$ordering=1;
		$orderby="title";
	}
	if($setting_ordering==4){
		$ordering=0;
		$orderby="modified_when";
	}
	if($setting_ordering==5){
		$ordering=1;
		$orderby="modified_when";
	}
	/*
	$fetch_content['ordering']
	0 - ascending position
	1 - descending position
	2 - ascending title
	3 - descending title
	4 - ascending modified_when
	5 - descending modified_when
	orderby:
	position=0
	title=1
	modified_when=2
	*/	
	//replace Ordering value with DESC or ASC
	if ($ordering == '0') {
		$ordering = 'ASC';
	} else {
		$ordering = 'DESC';
	}

	// Get total number of images
	$query_total_num = $database->query("SELECT image_id FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND active = '1' AND title != '' and group_id='0'");
	$total_num = $query_total_num->numRows();

	// Work-out if we need to add limit code to sql
	if($setting_images_per_page != 0) {
		$limit_sql = " LIMIT $position,$setting_images_per_page";
	} else {
		$limit_sql = "";
	}
	
	// Query images (for this page)
	$query_images = $database->query("SELECT image_id,title,link,description,copyright,modified_by,modified_when,extension,alttitle FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND active = '1' AND title != '' and group_id='0' ORDER BY $orderby $ordering".$limit_sql);
	$num_images = $query_images->numRows();
	
	// Create previous and next links
	if($setting_images_per_page != 0) {
		if($position > 0) {
			if(isset($_GET['g']) AND is_numeric($_GET['g'])) {
                $previous_url = '?p='.($position-$setting_images_per_page).'&amp;g='.$_GET['g'];
			} else {
                $previous_url = '?p='.($position-$setting_images_per_page);
			}
            
            $pl_prepend = '<a href="' . $previous_url . '">&laquo; ';
			$pl_append = '</a>';
			$previous_link = $pl_prepend . $GTEXT['PREVIOUS'] . $pl_append;
			$previous_page_link = $pl_prepend . $TEXT['PREVIOUS_PAGE'] . $pl_append;
		} else {
			$previous_link = '';
			$previous_page_link = '';
            $previous_url = '';
		}
		if($position+$setting_images_per_page >= $total_num) {
			$next_link = '';
			$next_page_link = '';
            $next_url = '';
		} else {
			if(isset($_GET['g']) AND is_numeric($_GET['g'])) {
                $next_url = '?p=' . ($position+$setting_images_per_page) . '&amp;g=' . $_GET['g'];
			} else {
                $next_url = '?p=' . ($position+$setting_images_per_page);
			}
            $nl_prepend = '<a href="' . $next_url . '"> ';
			$nl_append = ' &raquo;</a>';
			$next_link = $nl_prepend . $GTEXT['NEXT'] . $nl_append;
			$next_page_link = $nl_prepend . $TEXT['NEXT_PAGE'] . $nl_append;
		}
        
		if($position+$setting_images_per_page > $total_num) {
			$num_of = $position+$num_images;
		} else {
			$num_of = $position+$setting_images_per_page;
		}

        $page_current_no = ($position+1) . '-' . $num_of;
		$out_of = $page_current_no . ' '.strtolower($TEXT['OUT_OF']).' '.$total_num;
		$of     = $page_current_no . ' '.strtolower($TEXT['OF']).' '.$total_num;
		$display_previous_next_links = '';
	} else {
		$display_previous_next_links = 'none';
	}
		
	// Print header
    // If implementing new fields, don't forget to add a corresponding
    // description row into the table in modify_settings_help.php!
    $vars = array('[NEXT_PAGE_LINK]','[NEXT_LINK]','[NEXT_URL]', '[PREVIOUS_PAGE_LINK]', '[PREVIOUS_LINK]', '[PREVIOUS_URL]', '[PAGE_CURRENT_NO]', '[PAGE_TOTAL_NO]', '[OUT_OF]','[OF]','[DISPLAY_PREVIOUS_NEXT_LINKS]');
	if($display_previous_next_links == 'none') {
		$values = array('','','','', '', '', '', '', $display_previous_next_links);
	} else {
		$values = array($next_page_link, $next_link, $next_url, $previous_page_link, $previous_link, $previous_url, $page_current_no, $total_num, $out_of, $of, $display_previous_next_links);
	}
    echo str_replace($vars, $values, $setting_header);
	
	// Loop through and show images
	if($num_images > 0) {
		$counter = 0;
		while($image = $query_images->fetchRow()) {
			$uid = $image['modified_by']; // User who last modified the image
            $copyright = $image['copyright'];
			// Workout date and time of last modified image
			$image_date = gmdate(DATE_FORMAT, $image['modified_when']);
			$image_time = gmdate(TIME_FORMAT, $image['modified_when']);
			// Work-out the image link
			$image_link = LEPTON_URL.$image['link'].'.php';
			if(isset($_GET['p']) AND $position > 0) {
				$image_link .= '?p='.$position;
			}
			if(isset($_GET['g']) AND is_numeric($_GET['g'])) {
				if(isset($_GET['p']) AND $position > 0) {
					$image_link .= '&amp;';
				} else {
					$image_link .= '?';
				}
				$image_link .= 'g='.$_GET['g'];
			}
			// image file extension
			$ext = $image['extension'];
			// Check if we should show thumb or image or nothing at all
			if($setting_resize > 0) {
				if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/thumb".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext)) {
					$thumb = THUMB_PREPEND.LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/thumb".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext . '" alt="' . $image['alttitle'] . '" title="' . $image['alttitle'] . '" ' . THUMB_APPEND;
				} else {
					$thumb = '';
				}
			} else {
				if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext)) {
					$thumb = THUMB_PREPEND.LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext.'" alt="'.$image['alttitle'].'" title="'.$image['alttitle'].'" '.THUMB_APPEND;
				} else {
					$thumb = '';
				}
			}
            
            // Shall we replace empty Copyright fields with the default value?
            if (($copyright == "") && ($setting_copyright_auto == 2)) {
                $copyright = $setting_copyright_default;
            }

            // Replace vars with values
			$description=stripslashes($image['description']);
			$oLEPTON->preprocess($description);
            $vars = array('[PAGE_TITLE]', '[THUMB]', '[TITLE]', '[DESCRIPTION]', '[COPYRIGHT]', '[LINK]', '[DATE]', '[TIME]', '[USER_ID]', '[USERNAME]', '[DISPLAY_NAME]', '[EMAIL]', '[TEXT_READ_MORE]','[ALTTITLE]');
			if(isset($users[$uid]['username']) AND $users[$uid]['username'] != '') {
				$values = array(PAGE_TITLE, $thumb, stripslashes($image['title']), $description, $copyright, $image_link, $image_date, $image_time, $uid, $users[$uid]['username'], $users[$uid]['display_name'], $users[$uid]['email'], $TEXT['READ_MORE'],stripslashes($image['alttitle']));
			} else {
				$values = array(PAGE_TITLE, $thumb, stripslashes($image['title']), $description, $copyright, $image_link, $image_date, $image_time, '', '', '', '', $TEXT['READ_MORE'],stripslashes($image['alttitle']));
			}
			echo str_replace($vars, $values, $setting_image_loop);
			// Increment counter
			$counter = $counter+1;
			// Check if we should end this row
			if($counter == $setting_num_cols) {
				$counter = 0;
				echo NEW_ROW;
			}
		}
	}
	
	// Print footer
    // If implementing new fields, don't forget to add a corresponding
    // description row into the table in modify_settings_help.php!
    $vars = array('[NEXT_PAGE_LINK]','[NEXT_LINK]','[NEXT_URL]', '[PREVIOUS_PAGE_LINK]', '[PREVIOUS_LINK]', '[PREVIOUS_URL]', '[PAGE_CURRENT_NO]', '[PAGE_TOTAL_NO]', '[OUT_OF]','[OF]','[DISPLAY_PREVIOUS_NEXT_LINKS]');
	if($display_previous_next_links == 'none') {
		$values = array('','','','', '', '', '', '', $display_previous_next_links);
	} else {
		$values = array($next_page_link, $next_link, $next_url, $previous_page_link, $previous_link, $previous_url, $page_current_no, $total_num, $out_of, $of, $display_previous_next_links);
	}
    echo str_replace($vars, $values, $setting_footer);
	
} elseif(defined('IMAGE_ID') AND is_numeric(IMAGE_ID)) {
    // Display image page
	
	// Get settings
	$query_settings = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
	if($query_settings->numRows() > 0) {
		$fetch_settings = $query_settings->fetchRow();
		$setting_image_header = stripslashes($fetch_settings['image_header']);
		$setting_image_footer = stripslashes($fetch_settings['image_footer']);
        $setting_image_dir = trim($fetch_settings['image_dir'], '/');
        $setting_image_digits = $fetch_settings['image_digits'];
	} else {
		$setting_image_header = '';
		$setting_image_footer = '';
        $setting_image_dir    = '';
        $setting_image_digits = 1;
	}
	
	// Get page info
	$query_page = $database->query("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id = '".PAGE_ID."'");
	if($query_page->numRows() > 0) {
		$page = $query_page->fetchRow();
		$page_link = page_link($page['link']);
		if(isset($_GET['p']) AND $position > 0) {
			$page_link .= '?p='.$_GET['p'];
		}
	} else {
		exit('Page not found');
	}
	
	// Get total number of images
	$query_total_num = $database->query("SELECT image_id FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND active = '1' AND title != '' and group_id='0'");
	$total_num = $query_total_num->numRows();
    
	// Get image info
	$query_image = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '".IMAGE_ID."' AND active = '1'");
	if($query_image->numRows() > 0) {
		$image = $query_image->fetchRow();	
		$title = $image['title'];
        $modified_when = $image['modified_when'];
		$position = $image['position'];
		if((isset($_REQUEST['position'])and(isset($_REQUEST['group_id'])))){
			$group_id=$_REQUEST['group_id'];
			$squery="SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE image_id = '".$group_id."' AND active = '1'";
			$squery_image = $database->query($squery);
			$simage = $squery_image->fetchRow();	
			$stitle = $simage['title'];
			$smodified_when = $simage['modified_when'];
			$sposition = $simage['position'];
		} else {
			$stitle = $image['title'];
			$smodified_when = $image['modified_when'];
			$sposition = $image['position'];
		}
		$query_settings = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
		if($query_settings->numRows() > 0) {
			$fetch_settings = $query_settings->fetchRow();
			$setting_header = stripslashes($fetch_settings['header']);
			$setting_image_loop = stripslashes($fetch_settings['image_loop']);
			$setting_footer = stripslashes($fetch_settings['footer']);
			$setting_images_per_page = $fetch_settings['images_per_page'];
			$setting_num_cols = $fetch_settings['num_cols'];
			$setting_resize = stripslashes($fetch_settings['thmb_resize']);
			$setting_ordering = $fetch_settings['ordering'];
            $setting_image_dir = trim($fetch_settings['image_dir'], '/');
            $setting_image_digits = $fetch_settings['image_digits'];
            $setting_copyright_default = $fetch_settings['copyright_default'];
            $setting_copyright_auto = $fetch_settings['copyright_auto'];
		} else {
			$setting_header = '';
			$setting_image_loop = '';
			$setting_footer = '';
			$setting_images_per_page = '';
			$setting_num_cols = '';
			$setting_resize = '';
			$setting_ordering = '';
            $setting_image_dir = '';
            $setting_image_digits = 1;
            $setting_copyright_default = '';
            $setting_copyright_auto = 0;
		}
		if($setting_ordering==0){
			$ordering=0;
			$orderby="position";
			$orderby2=$sposition;
		}
		if($setting_ordering==1){
			$ordering=1;
			$orderby="position";
			$orderby2=$sposition;
		}
		if($setting_ordering==2){
			$ordering=0;
			$orderby="title";
			$orderby2=$stitle;
		}
		if($setting_ordering==3){
			$ordering=1;
			$orderby="title";
			$orderby2=$stitle;
		}
		if($setting_ordering==4){
			$ordering=0;
			$orderby="modified_when";
			$orderby2=$smodified_when;
		}
		if($setting_ordering==5){
			$ordering=1; 
			$orderby="modified_when";
			$orderby2=$smodified_when;
		}
		/*
		['ordering']
		0 - ascending position
		1 - descending position
		2 - ascending title
		3 - descending title
		4 - ascending modified_when
		5 - descending modified_when
		
		orderby:
		position=0
		title=1
		modified_when=2
		*/	
		if ($ordering == '0') {
			$ordering = 'ASC';
			$ordering2 = 'DESC';
			$asclt='<';
			$ascgt='>';
		} else {
			$ordering = 'DESC';
			$ordering2 = 'ASC';
			$asclt='>';
			$ascgt='<';
		}
		$ext = $image['extension'];
	
		//get actual position based on sort criteria
		$query_image_position = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND $orderby $asclt '$orderby2' AND active = '1' and group_id='0'");
		$position= $query_image_position->numRows()+1;
		if(isset($_REQUEST['position'])){
			$position=$_REQUEST['position'];
		}
	
		//to recalculate total num for inactive images	
		$query_total_num2 = $database->query("SELECT image_id FROM ".TABLE_PREFIX."mod_gallery_images WHERE section_id = '$section_id' AND $orderby $asclt '$orderby2' AND active = '0' and group_id='0'");
		$total_num2 = $query_total_num2->numRows();
	
		// Create previous and next links
		$query_surrounding = $database->query("SELECT image_id FROM ".TABLE_PREFIX."mod_gallery_images WHERE $orderby != '$orderby2' AND section_id = '$section_id'  AND active = '1' and group_id='0' LIMIT 1");
		if($query_surrounding->numRows() > 0) {
            $previous_url = '';
            $previous_link = '';
			// Get previous   < DESC
			if($position > 1) {
				$queryp="SELECT link,position FROM ".TABLE_PREFIX."mod_gallery_images WHERE $orderby $asclt '$orderby2' AND section_id = '$section_id'  AND active = '1'  and group_id='0' ORDER BY $orderby $ordering2 LIMIT 1";
				$query_previous = $database->query($queryp);
				$query_previous_count=$query_previous->numRows();
				if($query_previous_count >= 1) {
					$previous = $query_previous->fetchRow();
                    $previous_url = LEPTON_URL . $previous['link'] . '.php';
					$previous_link = '<a href="' . $previous_url . '">&laquo; '.$GTEXT['PREVIOUS'].'</a>';
				}
			}
			// Get next  >ASC
			if($position >= $total_num) {
                $next_url = '';
				$next_link = '';
			} else {
				$query_next = $database->query("SELECT link,position FROM ".TABLE_PREFIX."mod_gallery_images WHERE $orderby $ascgt '$orderby2' AND section_id = '$section_id'  AND active = '1' and group_id='0' ORDER BY $orderby $ordering LIMIT 1 ");
				if($query_next->numRows() >= 1) {
					$next = $query_next->fetchRow();
                    $next_url = LEPTON_URL . $next['link'] . '.php';
					$next_link = '<a href="' . $next_url . '"> '.$GTEXT['NEXT'].' &raquo;</a>';
				}
			}
		} else {
            $next_url = '';
			$next_link = '';
            $previous_url = '';
			$previous_link = '';
		}
		$out_of = $position.' '.strtolower($TEXT['OUT_OF']).' '.$total_num;
        $of = $position.' '.strtolower($TEXT['OF']).' '.$total_num;
        	
		// Show image
		$uid       = $image['modified_by']; // User who last modified the image
        $copyright = $image['copyright'];
	
		// Workout date and time of last modified image
		$image_date = gmdate(DATE_FORMAT, $image['modified_when']);
		$image_time = gmdate(TIME_FORMAT, $image['modified_when']);
        
        // Shall we replace empty Copyright fields with the default value?
        if (($copyright == '') && ($setting_copyright_auto == 2)) {
            $copyright = $setting_copyright_default;
        }        

		// Set vars and value
        // If implementing new fields, don't forget to add a corresponding
        // description row into the table in modify_settings_help.php!
		$description=stripslashes($image['description']);
		$oLEPTON->preprocess($description);
		$vars = array('[PAGE_TITLE]', '[TITLE]', '[DESCRIPTION]', '[COPYRIGHT]', '[BACK]', '[DATE]', '[TIME]', '[USER_ID]', '[USERNAME]', '[DISPLAY_NAME]', '[EMAIL]', '[PREVIOUS]', '[NEXT]', '[PREVIOUS_URL]', '[NEXT_URL]', '[PAGE_CURRENT_NO]', '[PAGE_TOTAL_NO]', '[OUT_OF]', '[OF]', '[TEXT_OUT_OF]', '[TEXT_OF]', '[TEXT_BACK]', '[ALTTITLE');
		if(isset($users[$uid]['username']) AND $users[$uid]['username'] != '') {
			$values = array(PAGE_TITLE, stripslashes($image['title']), $description, $copyright, $page_link, $image_date, $image_time, $uid, $users[$uid]['username'], $users[$uid]['display_name'], $users[$uid]['email'], $previous_link, $next_link, $previous_url, $next_url, $position, $total_num, $out_of, $of,  $TEXT['OUT_OF'], $TEXT['OF'], $TEXT['BACK'], stripslashes($image['alttitle']));
		} else {
			$values = array(PAGE_TITLE, stripslashes($image['title']), $description, $copyright, $page_link, $image_date, $image_time, '', '', '', '', $previous_link, $next_link, $previous_url, $next_url, $out_of, $of, $TEXT['OUT_OF'], $TEXT['OF'], $TEXT['BACK'], stripslashes($image['alttitle']));
		}
	
		// Print post header
		echo str_replace($vars, $values, $setting_image_header);
		$query_settings = $database->query("SELECT imagelink, subimage_footer, subimage_header  FROM ".TABLE_PREFIX."mod_gallery_settings WHERE section_id = '$section_id'");
		if($query_settings->numRows() > 0) {
			$fetch_settings = $query_settings->fetchRow();
			$setting_imagelink = $fetch_settings['imagelink'];
		} else {
			$setting_imagelink='';
		}
		$setting_imagelink = (int)$setting_imagelink;

		if($setting_imagelink==2){
			$target='target="_new"';
		}
		if($setting_imagelink==1){
			$target='target="_parent"';
		}
		if($setting_imagelink==0){
			$target='';
		}

		// Some mojo to see if we should use the 'main' image
		if (file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/main".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext)) {
			// Show image w/ link to full size pic
			if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext)) {
				//View in PopUp
				if($setting_imagelink==3){
					$info=getimagesize(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext);
					$width=$info[0]+35;
					$height=$info[1]+35;
					if($width>1024) $width=1024;
					if($height>768) $height=768;
					echo"<a href=\"javascript:void(0);\" onClick=\"javascript:pop=window.open('" . LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext . "','".stripslashes($image['title'])."','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=".$width.",height=".$height.",left=125,top=100'); return false;\">";
				}elseif($setting_imagelink!=0){
					echo "<a href=\"" . LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext . "\" title=\"".$image['alttitle']."\" $target>";
				}
				echo THUMB_PREPEND.LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/main".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext.'" alt="'.$image['alttitle'].'" title="'.$image['alttitle'].'" '.THUMB_APPEND;
				if($setting_imagelink!=0){
					echo "</a>";
				}
			}	
		} else {
			// Show image
			if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext)) {
				echo THUMB_PREPEND.LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/image".sprintf("%0${setting_image_digits}u", $image['image_id']).'.'.$ext.THUMB_APPEND;
			}
		}
        echo "\n";
		if(isset($_REQUEST['group_id'])){
			$imageid=$_REQUEST['group_id'];
		} else {
			$imageid=$image['image_id'];
		}	
        
        // Show thumbs of subimages (if there are any)
		$query_subimage = $database->query("SELECT * FROM ".TABLE_PREFIX."mod_gallery_images WHERE (group_id = '".$imageid."' or image_id='".$imageid."') AND active = '1' ORDER BY group_id asc,$orderby $ordering ");
		if($query_subimage->numRows() > 1) {
			echo str_replace($vars, $values, stripslashes($fetch_settings['subimage_header']));
			while($subimage = $query_subimage->fetchRow()) {
				$subtitle = $subimage['title'];
				$subext = $subimage['extension'];
				$subposition = $subimage['position'];
				if(trim($subimage['3']=="0")){
					$imagegroupid=$subimage['image_id'];
				} else {
					$imagegroupid=$subimage['group_id'];
				}
				echo '    <a href="'.LEPTON_URL.$subimage['link'].'.php?group_id='.$imagegroupid.'&amp;position='.$position.'">';
				echo THUMB_PREPEND.LEPTON_URL.MEDIA_DIRECTORY."/$setting_image_dir/thumb".sprintf("%0${setting_image_digits}u", $subimage['image_id']).'.'.$subext.'" alt="'.$subimage['alttitle'].'" title="'.$subimage['alttitle'].'" '.THUMB_APPEND;
				echo "</a>\n";
			}	
			echo  str_replace($vars, $values, $fetch_settings['subimage_footer']);
		}
		// Print post footer
		echo str_replace($vars, $values, $setting_image_footer);
	} else {
		header('Location: ' . LEPTON_URL . PAGES_DIRECTORY . '/');
	}
}

?>
